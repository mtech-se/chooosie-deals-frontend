import { LocalStorageConstants } from '../../constants/LocalStorageConstants';
import { CLEAR_APP_DATA, INIT_STORE, SET_CUSTOMER_AND_ACCESS_TOKEN, UPDATE_SEARCH_TERM } from '../actions/action-types';
import { initialState } from '../store';

const rootReducer = (state = initialState, action) => {

  if (action.type === SET_CUSTOMER_AND_ACCESS_TOKEN) {
    const accessToken = action.payload.accessToken;
    const accessTokenPrefix = action.payload.accessTokenPrefix;

    localStorage.setItem(LocalStorageConstants.ACCESS_TOKEN, accessToken);
    localStorage.setItem(LocalStorageConstants.ACCESS_TOKEN_PREFIX, accessTokenPrefix);

    return Object.assign({}, state, {
      customer: action.payload.customer,
      accessToken,
      accessTokenPrefix,
    });
  }

  if (action.type === INIT_STORE) {
    return Object.assign({}, state, {
      accessToken: action.payload.accessToken,
      accessTokenPrefix: action.payload.accessTokenPrefix,
      customer: action.payload.customer,
    });
  }

  if (action.type === CLEAR_APP_DATA) {
    return {};
  }

  if (action.type === UPDATE_SEARCH_TERM) {
    return Object.assign({}, state, {
      searchTerm: action.payload,
    });
  }

  return state;
};

export default rootReducer;