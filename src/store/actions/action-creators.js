import { LocalStorageConstants } from '../../constants/LocalStorageConstants';
import { CustomerFactory } from '../../factories/CustomerFactory';
import { LoginService } from '../../services/LoginService';
import { UserService } from '../../services/UserService';
import { CLEAR_APP_DATA, INIT_STORE, SET_CUSTOMER_AND_ACCESS_TOKEN, UPDATE_SEARCH_TERM } from './action-types';

export const logInCustomer = (email, password) => function (dispatch) {
  return LoginService.login(email, password)
    .then(response => {
      const accessToken = response.data.access_token;
      const accessTokenPrefix = response.data.access_token_prefix;
      const customer = CustomerFactory.createFromJson(response.data.user);

      return {
        accessToken,
        accessTokenPrefix,
        customer,
      };
    })
    .then(payload => dispatch({
      type: SET_CUSTOMER_AND_ACCESS_TOKEN,
      payload
    }));
};

export const initStoreWithAccessTokenAndUpdateCustomerDetails = () => async function (dispatch) {
  const accessToken = localStorage.getItem(LocalStorageConstants.ACCESS_TOKEN);
  const accessTokenPrefix = localStorage.getItem(LocalStorageConstants.ACCESS_TOKEN_PREFIX);

  // addRequestInterceptorToAxios(accessToken, accessTokenPrefix);
  // addResponseInterceptorToAxios();

  const response = await UserService.getMyDetails();
  const customer = CustomerFactory.createFromJson(response.data);

  dispatch({
    type: INIT_STORE,
    payload: {
      accessToken,
      accessTokenPrefix,
      customer,
    },
  });
};

export const clearAppData = () => {
  localStorage.clear();

  return {
    type: CLEAR_APP_DATA,
  };
};

export const updateSearchTerm = payload => ({
  type: UPDATE_SEARCH_TERM,
  payload
});