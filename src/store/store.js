import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/reducers';

export const initialState = {
  accessToken: undefined,
  accessTokenPrefix: undefined,
  customer: undefined,

  searchTerm: '',
};

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  storeEnhancers(applyMiddleware(thunk)),
);

// store.subscribe(() => console.log('Look ma, Redux!!'));

export default store;