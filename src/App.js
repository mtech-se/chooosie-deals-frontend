import React, { Component } from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import { history } from './history';
import { updateSearchTerm } from './store/actions/action-creators';
import store from './store/store';
import { AuthBaseView } from './views/AuthBaseView';
import { CheckoutSuccessView } from './views/CheckoutSuccessView/CheckoutSuccessView';
import { CheckoutView } from './views/CheckoutView/CheckoutView';
import { DealDetailsView } from './views/DealDetailsView/DealDetailsView';
import Error404 from './views/Errors/Error404';
import { GuestBaseView } from './views/GuestBaseView';
import { HomeView } from './views/HomeView/HomeView';
import { LoginView } from './views/LoginView/LoginView';
import { LogoutView } from './views/LogoutView/LogoutView';
import { MyAccountView } from './views/MyAccountView/MyAccountView';
import { NoChromeGuestBaseView } from './views/NoChromeGuestBaseView';
import { RegisterView } from './views/RegisterView/RegisterView';
import { SearchResultsView } from './views/SearchResultsView/SearchResultsView';

const resetStoreSearchTermForAllRoutesExcept = location => {
  const pathname = location.pathname;
  if (pathname !== '/deals') store.dispatch(updateSearchTerm(''));
};

export class App extends Component {
  constructor(props) {
    super(props);

    history.listen(resetStoreSearchTermForAllRoutesExcept);
  }

  render() {
    return (
      <React.Fragment>
        <Router history={history}>
          <Switch>
            <Route exact
                   path="/login"
                   render={() => <NoChromeGuestBaseView component={<LoginView/>}/>}/>

            <Route exact
                   path="/register"
                   render={() => <NoChromeGuestBaseView component={<RegisterView/>}/>}/>

            <Route exact
                   path="/"
                   render={() => <GuestBaseView component={<HomeView/>}/>}/>

            <Route exact
                   path="/deals/:dealId"
                   render={props => <GuestBaseView component={<DealDetailsView {...props}/>}/>}/>

            <Route exact
                   path="/deals"
                   render={props => <GuestBaseView component={<SearchResultsView {...props}/>}/>}/>

            <Route exact
                   path="/checkout"
                   render={() => <AuthBaseView component={<CheckoutView/>}/>}/>

            <Route exact
                   path="/checkout/success"
                   render={() => <AuthBaseView component={<CheckoutSuccessView/>}/>}/>

            <Route exact
                   path="/my-account"
                   render={() => <AuthBaseView component={<MyAccountView/>}/>}
            />

            {/*<Route exact path="/my-purchase-history" render={() => <BaseView component={<MyPurchaseHistoryView/>}/>}/>*/}

            <Route exact
                   path="/logout"
                   render={() => <LogoutView/>}/>

            <Route exact
                   path="/errors/404"
                   component={Error404}/>

            <Route component={Error404}/>
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}
