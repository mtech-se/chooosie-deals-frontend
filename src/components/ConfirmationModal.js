import * as PropTypes from 'prop-types';
import React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

export const ConfirmationModal = props => (
  <Modal isOpen={props.shouldShowModal}>
    <ModalHeader>{props.title}</ModalHeader>
    <ModalBody>{props.body}</ModalBody>
    <ModalFooter>
      <Button color="primary"
              onClick={props.onPrimaryClick}>OK</Button>{' '}
      <Button color="secondary"
              onClick={props.onSecondaryClick}>Cancel</Button>
    </ModalFooter>
  </Modal>
);

ConfirmationModal.propTypes = {
  shouldShowModal: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  onPrimaryClick: PropTypes.func.isRequired,
  onSecondaryClick: PropTypes.func.isRequired,
};