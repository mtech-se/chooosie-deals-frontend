import { Grid } from '@material-ui/core/es/index';
import React, { Component } from 'react';
import { DealFactory } from '../../factories/DealFactory';
import { DealService } from '../../services/DealService';
import { BaseProgressLoader } from '../BaseProgressLoader';
import { DealCardHero } from '../DealCard/DealCardHero';
import { NoRecordsFound } from '../NoRecordsFound';

export class DealsOfTheMonth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasFetchedData: false,
      deals: undefined,
      indexOfDealToDisplay: 0,
    };

    this.incrementIndexOfDealToDisplay = this.incrementIndexOfDealToDisplay.bind(this);

    this.carouselTimer = setInterval(this.incrementIndexOfDealToDisplay, 4000);
  }

  incrementIndexOfDealToDisplay() {
    const currentIndex = this.state.indexOfDealToDisplay;
    const maxIndex = this.state.deals.length - 1;

    this.setState({
      indexOfDealToDisplay: currentIndex === maxIndex ? 0 : currentIndex + 1,
    });
  }

  async componentDidMount() {
    const dealsResponse = await DealService.getDealsOfTheMonth();
    const deals = DealFactory.createFromJsonArray(dealsResponse.data.content);
    this.setState({
      hasFetchedData: true,
      deals,
    });
  }

  componentWillUnmount() {
    clearInterval(this.carouselTimer);
  }

  render() {
    if (this.state.hasFetchedData === false) return <BaseProgressLoader/>;
    if (this.state.deals.length === 0) return <NoRecordsFound itemLabel={'DOTMs'}/>;

    return (
      <Grid container
            justify={'center'}>
        <Grid item
              xs={12}
              style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>

          <div>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <DealCardHero
                deal={this.state.deals[this.state.indexOfDealToDisplay]}
              />
            </div>
          </div>

        </Grid>
      </Grid>
    );
  }
}