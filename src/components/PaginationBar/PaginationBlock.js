import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ColorConstants } from '../../constants/ColorConstants';
import { StyleConstants } from '../../constants/StyleConstants';

export class PaginationBlock extends Component {
  render() {
    if (this.props.linkUrl !== undefined && this.props.onClick === undefined) {
      throw new Error('Pagination block has link URL but no click handler given as prop!');
    }

    const backgroundColor = this.props.isShaded ? ColorConstants.transparentBlack : '#f9f9f9';
    const borderRightWidth = this.props.isLast ? '1px' : '0';
    const borderTopRightRadius = this.props.isLast ? '4px' : '0';
    const borderBottomRightRadius = this.props.isLast ? '4px' : '0';
    const borderTopLeftRadius = this.props.isFirst ? '4px' : '0';
    const borderBottomLeftRadius = this.props.isFirst ? '4px' : '0';
    const padding = this.props.isMiddle ? '8px 32px' : '8px';

    const contents = (
      <div
        onClick={this.props.isDisabled ? undefined : this.props.onClick}
        style={{
          backgroundColor,
          color: this.props.isDisabled ? ColorConstants.transparentBlack : ColorConstants.teal,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          minWidth: '40px',
          minHeight: '40px',
          padding,
          borderColor: 'rgba(13, 162, 151, 0.2)',
          borderStyle: 'solid',
          borderTopWidth: '1px',
          borderBottomWidth: '1px',
          borderLeftWidth: '1px',
          borderRightWidth,
          borderTopRightRadius,
          borderBottomRightRadius,
          borderTopLeftRadius,
          borderBottomLeftRadius,
          fontFamily: StyleConstants.fontFamily,
        }}
      >
        {this.props.content}
      </div>
    );

    let renderElement;
    if (this.props.linkUrl === undefined) {
      renderElement = contents;
    } else {
      renderElement = (
        <Link
          to={this.props.linkUrl}
          style={{
            textDecoration: 'none',
          }}
        >
          {contents}
        </Link>
      );
    }

    return renderElement;
  }
}

PaginationBlock.propTypes = {
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element
  ]),
  isShaded: PropTypes.bool,
  isFirst: PropTypes.bool,
  isMiddle: PropTypes.bool,
  isLast: PropTypes.bool,
  linkUrl: PropTypes.string,
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool,
};

PaginationBlock.defaultProps = {
  isShaded: false,
  isFirst: false,
  isMiddle: false,
  isLast: false,
  linkUrl: undefined,
  isDisabled: false,
};