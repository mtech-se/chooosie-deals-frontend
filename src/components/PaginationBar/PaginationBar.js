import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import * as PropTypes from 'prop-types';
import * as queryString from 'query-string';
import React, { Component } from 'react';
import { PaginationRequestParams } from '../../models/PaginationRequestParams';
import { SearchParams } from '../../models/SearchParams';
import { PaginationBlock } from './PaginationBlock';

export class PaginationBar extends Component {
  render() {
    const numPages = Math.ceil(this.props.numRecordsInTotal / this.props.numRecordsPerPage);

    const prevPaginationParams = this.props.currentPageNumber === 0 ? undefined :
      new PaginationRequestParams(this.props.numRecordsPerPage, this.props.currentPageNumber - 1);

    const nextPaginationParams = this.props.currentPageNumber === numPages - 1 ? undefined :
      new PaginationRequestParams(this.props.numRecordsPerPage, this.props.currentPageNumber + 1);

    return (
      <div style={{
        display: 'flex',
      }}>
        <PaginationBlock
          content={<ChevronLeft/>}
          linkUrl={prevPaginationParams === undefined ? undefined : 'deals?' + queryString.stringify({ ...this.props.searchParams, ...prevPaginationParams })}
          isFirst
          onClick={() => this.props.onPaginationNavigation(prevPaginationParams)}
          isDisabled={prevPaginationParams === undefined}
        />

        <PaginationBlock
          content={this.props.currentPageNumber + 1 + ' of ' + numPages}
          isMiddle
        />

        <PaginationBlock
          content={<ChevronRight/>}
          linkUrl={nextPaginationParams === undefined ? undefined : 'deals?' + queryString.stringify({ ...this.props.searchParams, ...nextPaginationParams })}
          isLast
          onClick={() => this.props.onPaginationNavigation(nextPaginationParams)}
          isDisabled={nextPaginationParams === undefined}
        />
      </div>
    );
  }
}

PaginationBar.propTypes = {
  numRecordsInTotal: PropTypes.number,
  numRecordsPerPage: PropTypes.number,
  currentPageNumber: PropTypes.number,
  searchParams: PropTypes.instanceOf(SearchParams),
  onPaginationNavigation: PropTypes.func.isRequired,
};