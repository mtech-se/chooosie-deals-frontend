import PropTypes from 'prop-types';
import React from 'react';
import { Order } from '../../models/Order/Order';
import { MyOrderService } from '../../services/MyOrderService';
import { ShoppingCartListItem } from '../ShoppingCartListItem/ShoppingCartListItem';

export class ShoppingCartList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      amendedOrder: undefined,
      orderItems: undefined,
    };

    this.removeOrderItem = this.removeOrderItem.bind(this);
  }

  componentDidMount() {
    const orderItems = this.props.currentOrder.orderItems;

    this.setState({ orderItems });
  }

  async removeOrderItem(orderItem) {
    const amendedOrder = this.props.currentOrder;
    amendedOrder.removeOrderItem(orderItem);

    this.setState({
      orderItems: amendedOrder.orderItems,
    }, async () => {
      let amendedOrderItems = [];
      for (let i = 0; i < amendedOrder.orderItems.length; i++) {
        amendedOrderItems.push({
            'deal': {
              'id': amendedOrder.orderItems[i].deal.id
            }
          }
        );
      }

      const requestBody = {
        'order_items': amendedOrderItems
      };

      const responseUpdateOrder = await MyOrderService.updateOrder(amendedOrder.id, requestBody);

      if (responseUpdateOrder.status === 200) this.props.orderUpdated();
      else throw new Error('Something went wrong - the response was something other than 200');

    });
  }

  render() {
    let shoppingCartListItem = undefined;

    if (this.state.orderItems) {
      shoppingCartListItem = this.state.orderItems.map(function (orderItem) {
        return (
          <div key={orderItem.id}>
            <ShoppingCartListItem
              orderItem={orderItem}
              removeOrderItem={this.removeOrderItem}
            />
          </div>
        );
      }, this);
    }

    return (
      <div>
        {shoppingCartListItem}
      </div>
    );
  }
}

ShoppingCartList.propTypes = {
  currentOrder: PropTypes.instanceOf(Order),
  orderUpdated: PropTypes.func
};