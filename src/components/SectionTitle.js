import * as PropTypes from 'prop-types';
import React from 'react';

export const SectionTitle = props => (
  <span style={{
    fontFamily: 'Libre Franklin, sans-serif',
    fontSize: '20px',
    fontWeight: '700',
    color: '#36173b',
    textTransform: 'uppercase',
  }}>{props.title}</span>
);

SectionTitle.propTypes = {
 title: PropTypes.string.isRequired
};