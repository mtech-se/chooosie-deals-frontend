import { produce } from 'immer';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Customer } from '../../models/Customer';
import { CustomerService } from '../../services/CustomerService';
import { initStoreWithAccessTokenAndUpdateCustomerDetails } from '../../store/actions/action-creators';
import ObjectUtils from '../../utils/ObjectUtils/ObjectUtils';
import { ActionButton } from '../../views/DealDetailsView/ActionButton';
import { LabelField } from '../LabelField/LabelField';
import { PanelTitle } from '../PanelTitle/PanelTitle';

class BaseMyAccountDetails extends Component {
  constructor(props) {
    super(props);

    const customer = new Customer();
    ObjectUtils.mapValues(this.props.initialCustomer, customer);

    this.state = {
      isDisabled: false,
      customer,
    };

    this.saveChanges = this.saveChanges.bind(this);
  }

  onFormContentChange(field, value) {
    const state = produce(this.state, draft => {
      draft.customer[field] = value;
    });

    this.setState(state);
  }

  saveChanges() {
    this.setState({
      disableButton: true,
      // }, async () => {
    }, async () => {
      const requestBody = {
        first_name: this.state.customer.firstName,
        last_name: this.state.customer.lastName,
        gender: this.state.customer.gender,
        billing_first_name: this.state.customer.billingFirstName,
        billing_last_name: this.state.customer.billingLastName,
        billing_cellphone: this.state.customer.billingCellphone,
        billing_telephone: this.state.customer.billingTelephone,
        billing_add_line_one: this.state.customer.billingAddressLineOne,
        billing_add_line_two: this.state.customer.billingAddressLineTwo,
        billing_add_postal_code: this.state.customer.billingAddressPostalCode,
      };

      const response = await CustomerService.updateCustomer(this.state.customer.id, requestBody);
      if (response.status !== 200) throw new Error('Something went wrong with updating customer details');
      this.props.initStoreWithAccessTokenAndUpdateCustomerDetails();
    });
  }

  render() {
    if (this.props.initialCustomer === undefined) return null;

    return <div>
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        marginBottom: '16px'
      }}>
        <div style={{
          width: '50%',
          marginRight: '8px',
        }}>
          <PanelTitle title={'Account Information'}/>

          <div style={{
            width: '90%'
          }}>
            <LabelField
              title={'First Name'}
              type={'text'}
              value={this.state.customer.firstName}
              onChangeHandler={event => this.onFormContentChange('firstName', event.target.value)}
              required={true}
            />

            <LabelField
              title={'Last Name'}
              type={'text'}
              value={this.state.customer.lastName}
              onChangeHandler={event => this.onFormContentChange('lastName', event.target.value)}
              required={true}
            />

            <LabelField
              title={'Email'}
              type={'email'}
              value={this.state.customer.email}
              onChangeHandler={event => this.onFormContentChange('email', event.target.value)}
              required={true}
              isDisabled={true}
            />
          </div>
        </div>

        <div style={{
          width: '50%',
        }}>

          <PanelTitle title={'Billing Information'}/>

          <div style={{
            width: '90%'
          }}>

            <LabelField
              title={'First Name'}
              type={'text'}
              value={this.state.customer.billingFirstName}
              onChangeHandler={event => this.onFormContentChange('billingFirstName', event.target.value)}
              required={true}
            />

            <LabelField
              title={'Last Name'}
              type={'text'}
              value={this.state.customer.billingLastName}
              onChangeHandler={event => this.onFormContentChange('billingLastName', event.target.value)}
              required={true}
            />

            <LabelField
              title={'Cellphone Number'}
              type={'text'}
              value={this.state.customer.billingCellphone}
              onChangeHandler={event => this.onFormContentChange('billingCellphone', event.target.value)}
              required={true}
            />

            <LabelField
              title={'Telephone Number'}
              type={'text'}
              value={this.state.customer.billingTelephone}
              onChangeHandler={event => this.onFormContentChange('billingTelephone', event.target.value)}
              required={true}
            />

            <LabelField
              title={'Address (Line 1)'}
              type={'text'}
              value={this.state.customer.billingAddressLineOne}
              onChangeHandler={event => this.onFormContentChange('billingAddressLineOne', event.target.value)}
              required={true}
            />

            <LabelField
              title={'Address (Line 2)'}
              type={'text'}
              value={this.state.customer.billingAddressLineTwo}
              onChangeHandler={event => this.onFormContentChange('billingAddressLineTwo', event.target.value)}
              required={true}
            />

            <LabelField
              title={'Postal Code'}
              type={'text'}
              value={this.state.customer.billingAddressPostalCode}
              onChangeHandler={event => this.onFormContentChange('billingAddressPostalCode', event.target.value)}
              required={true}
            />
          </div>
        </div>
      </div>
      <div
        style={{
          display: 'flex',
        }}>
        <ActionButton
          label={'Save Changes'}
          color={'#eeeeee'}
          backgroundColor={'#601e59'}
          fontSize={'18px'}
          disabled={this.state.isDisabled}
          onClick={this.saveChanges}
        />
      </div>
    </div>;
  }
}

BaseMyAccountDetails.propTypes = {
  initialCustomer: PropTypes.instanceOf(Customer),
  initStoreWithAccessTokenAndUpdateCustomerDetails: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  initialCustomer: state.customer,
});

const mapDispatchToProps = dispatch => ({
  initStoreWithAccessTokenAndUpdateCustomerDetails: () => dispatch(initStoreWithAccessTokenAndUpdateCustomerDetails()),
});

export const MyAccountDetails = connect(mapStateToProps, mapDispatchToProps)(BaseMyAccountDetails);