import { AppBar } from '@material-ui/core/es/index';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { Customer } from '../../models/Customer';
import { AppBrand } from '../AppBrand/AppBrand';
import { ProfileMenu } from '../ProfileMenu/ProfileMenu';
import { SearchBar } from '../SearchBar';
import { ShoppingCartMenu } from '../ShoppingCartMenu';

export class BaseTopNavBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      height: 'initial',
      showCountrySelect: true,
      showProfileMenu: true,
      showProfileMenuDropdown: false,
      showShoppingCartIcon: true,
      showOverflowIcon: false,
      appBrandRightMargin: 'initial',
    };

    this.onResize = this.onResize.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
    this.onResize();
  }

  onResize() {
    if (window.innerWidth < 960) {
      this.setState({
        height: '52px',
        showCountrySelect: false,
        showProfileMenu: false,
        showShoppingCartIcon: false,
        showOverflowIcon: true,
        appBrandRightMargin: '4px',
      });
    } else {
      this.setState({
        height: '104px',
        showCountrySelect: true,
        showProfileMenu: true,
        showShoppingCartIcon: true,
        showOverflowIcon: false,
        appBrandRightMargin: '16px',
      });
    }
  }

  render() {
    return (
      <AppBar position="static">
        <div style={{
          backgroundColor: '#b21b76',
          height: this.state.height,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          <AppBrand
            imageHeight={this.state.height === '52px' ? '32px' : '64px'}
            style={{
              marginRight: this.state.appBrandRightMargin,
            }}
          />

          <SearchBar style={{
            marginLeft: '4px',
            marginRight: '16px',
          }}
          />

          {/*{this.state.showCountrySelect &&*/}
          {/*<CountrySelect style={{ marginLeft: '16px', marginRight: '16px' }}/>*/}
          {/*}*/}

          {this.state.showShoppingCartIcon && this.props.customer !== undefined &&
          <ShoppingCartMenu/>
          }

          {this.state.showProfileMenu &&
          <ProfileMenu/>
          }

          {/*{this.state.showOverflowIcon &&*/}
          {/*<MoreVertIcon style={{ fontSize: '32px' }}/>*/}
          {/*}*/}
        </div>
      </AppBar>
    );
  }
}

BaseTopNavBar.propTypes = {
  customer: PropTypes.instanceOf(Customer),
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

const mapStateToProps = state => ({
  customer: state.customer,
});

export const TopNavBar = connect(mapStateToProps)(withRouter(BaseTopNavBar));