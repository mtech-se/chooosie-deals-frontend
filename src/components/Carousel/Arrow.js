import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import * as PropTypes from 'prop-types';
import React from 'react';

export const Arrow = props => {
  const defaultArrowStyle = {
    fontSize: '80px',
    color: props.disable ? 'black' : '#b21b76',
    cursor: 'pointer',
    visibility: props.disable ? 'hidden' : 'visible',
  };

  switch (props.direction) {
    case 'left':
      return (
        <ChevronLeft
          style={defaultArrowStyle}
          onClick={props.onClick}
        />
      );
    case 'right':
      return (
        <ChevronRight
          style={defaultArrowStyle}
          onClick={props.onClick}
        />
      );
    default:
      throw new Error('Unknown arrow direction');
  }

};

Arrow.propTypes = {
  direction: PropTypes.oneOf(['left', 'right']).isRequired,
  onClick: PropTypes.func.isRequired,
  disable: PropTypes.bool,
};