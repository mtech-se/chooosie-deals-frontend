import { Grid } from '@material-ui/core';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Deal } from '../../models/Deal';
import { DealCardPortrait } from '../DealCard/DealCardPortrait';
import { SectionTitle } from '../SectionTitle';
import { Arrow } from './Arrow';

export class Carousel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      numItemsInBackwardDirection: undefined,
      numItemsInForwardDirection: undefined,
      currentCursorPosition: undefined,
    };

    this.onArrowLeftClick = this.onArrowLeftClick.bind(this);
    this.onArrowRightClick = this.onArrowRightClick.bind(this);
  }

  componentDidMount() {
    const candidateNumItemsInForwardDirection = this.props.deals.length - this.props.numItemsToShow;
    const numItemsInForwardDirection = Math.max(candidateNumItemsInForwardDirection, 0);

    this.setState({
      numItemsInBackwardDirection: 0,
      numItemsInForwardDirection,
      currentCursorPosition: 0,
    });
  }

  componentDidUpdate(prevProps) {
    const numNewDealsFetched = this.props.deals.length - prevProps.deals.length;
    if (numNewDealsFetched > 0) this.setState({ numItemsInForwardDirection: this.state.numItemsInForwardDirection + numNewDealsFetched });
  }

  getLeftPositionInPixels() {
    return this.state.currentCursorPosition * (this.props.itemWidth + this.props.marginBetweenItems);
  }

  onArrowLeftClick() {
    this.setState({
      currentCursorPosition: this.state.currentCursorPosition + this.props.numItemsToScrollPerClick,
      numItemsInForwardDirection: this.state.numItemsInForwardDirection + this.props.numItemsToScrollPerClick,
      numItemsInBackwardDirection: this.state.numItemsInBackwardDirection - this.props.numItemsToScrollPerClick,
    });
  }

  onArrowRightClick() {
    this.setState({
      currentCursorPosition: this.state.currentCursorPosition - this.props.numItemsToScrollPerClick,
      numItemsInForwardDirection: this.state.numItemsInForwardDirection - this.props.numItemsToScrollPerClick,
      numItemsInBackwardDirection: this.state.numItemsInBackwardDirection + this.props.numItemsToScrollPerClick,
    }, () => {
      if (this.state.numItemsInForwardDirection <= this.props.numItemsToShow) this.props.onFetchMore();
    });
  }

  render() {
    const carouselWidth = this.props.numItemsToShow * this.props.itemWidth + (this.props.numItemsToShow - 1) * this.props.marginBetweenItems;

    const items = [];
    this.props.deals.forEach((deal, index) => {
      const rightMargin = index === this.props.deals.length - 1 ? '0' : '16px';
      items.push(
        <DealCardPortrait
          key={deal.id}
          deal={deal}
          rightMargin={rightMargin}
          shouldAnimate={this.state.shouldAnimate}
        />);
    });

    return (
      <Grid item
            xs={12}
            style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <Arrow
          direction={'left'}
          onClick={this.onArrowLeftClick}
          disable={this.state.numItemsInBackwardDirection === 0}
        />

        <div>
          <div style={{ marginBottom: '16px' }}>
            <SectionTitle title={this.props.label}/>
          </div>

          <div style={{
            width: carouselWidth + 'px',
            overflow: 'hidden',
          }}>
            <div style={{
              display: 'flex',
              position: 'relative',
              left: this.getLeftPositionInPixels() + 'px',
              transition: '0.2s',
            }}>
              {items}
            </div>
          </div>
        </div>

        <Arrow
          direction={'right'}
          onClick={this.onArrowRightClick}
          disable={this.state.numItemsInForwardDirection <= 0}
        />
      </Grid>
    );
  }
}

Carousel.propTypes = {
  label: PropTypes.string.isRequired,
  deals: PropTypes.arrayOf(PropTypes.instanceOf(Deal)).isRequired,
  numItemsToShow: PropTypes.number.isRequired,
  numItemsToScrollPerClick: PropTypes.number.isRequired,
  itemWidth: PropTypes.number.isRequired,
  marginBetweenItems: PropTypes.number.isRequired,
  onFetchMore: PropTypes.func.isRequired,
};