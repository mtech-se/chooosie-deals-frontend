import Typography from '@material-ui/core/es/Typography/Typography';
import PropTypes from 'prop-types';
import React from 'react';

export const TabContainer = props => {
  return (
    <Typography
      component="div"
      style={{
        padding: 8
      }}
    >
      {props.children}
    </Typography>
  );
};

TabContainer.propTypes = {
  children: PropTypes.node,
};
