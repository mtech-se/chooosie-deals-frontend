import React, { Component } from 'react';
import { OrderFactory } from '../../factories/OrderFactory';
import { MyOrderService } from '../../services/MyOrderService';
import { BaseProgressLoader } from '../BaseProgressLoader';
import { OrderHistoryItem } from '../OrderHistoryItem/OrderHistoryItem';
import { PanelTitle } from '../PanelTitle/PanelTitle';

export class MyOrderHistory extends Component {
  constructor(props) {
    super(props);

    this.state = {
      allPaidOrders: undefined,
      isMakingNetworkCall: false
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await MyOrderService.getAllOrders();
      const allOrders = OrderFactory.createFromJsonArray(response.data.content);

      let allPaidOrders = [];

      if (allOrders) {
        for (let order of allOrders) {
          if (order.payments.length !== 0) {
            allPaidOrders.push(order);
          }
        }
      }

      this.setState({
        allPaidOrders,
        isMakingNetworkCall: false,
      });
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <BaseProgressLoader/>;

    let paidOrders = undefined;

    if (this.state.allPaidOrders) {
      paidOrders = this.state.allPaidOrders.map(order => (
        <div key={order.id}>
          {order.orderItems.map(orderItem => (
            <div key={orderItem.id}>
              <OrderHistoryItem
                order={order}
                orderItem={orderItem}
                onOrderItemRedeemed={() => this.fetchData()}
              />
            </div>
          ))}
        </div>
      ));
    }

    return (
      <div style={{
        fontFamily: 'Roboto, sans-serif',
      }}>
        <PanelTitle
          title={'Order History'}
          marginBottom={'16px'}
        />
        {paidOrders}
      </div>
    );
  }
}