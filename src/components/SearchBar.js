import { Search as SearchIcon } from '@material-ui/icons';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { SearchParams } from '../models/SearchParams';
import { updateSearchTerm } from '../store/actions/action-creators';
import { getSearchParamsFromQueryString } from '../utils/UrlUtils';

class BaseSearchBar extends Component {
  constructor(props) {
    super(props);

    const searchParams = getSearchParamsFromQueryString(props.location.search);
    if (searchParams.searchTerm !== undefined) props.onSearchTermChange(searchParams.searchTerm);

    this.state = {
      hintText: '',
      width: 'initial',
      height: 'initial',
      searchIconSize: 'initial',
      shouldRedirect: false,
    };

    this.onResize = this.onResize.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
    this.onResize();
  }

  onResize() {
    if (window.innerWidth < 960) {
      this.setState({
        hintText: 'Search',
        width: '240px',
        height: '36px',
        searchIconSize: '24px',
      });
    } else {
      this.setState({
        hintText: 'Search for places, offers & more',
        width: '524px',
        height: '48px',
        searchIconSize: '32px',
      });
    }
  }

  onSubmit(event) {
    event.preventDefault();

    this.setState({
      shouldRedirect: true,
    });
  }

  componentDidUpdate() {
    if (this.state.shouldRedirect === true) {
      this.setState({
        shouldRedirect: false,
      });
    }
  }

  render() {
    if (this.state.shouldRedirect === true) {
      const searchParams = new SearchParams();
      searchParams.searchTerm = this.props.searchTerm;

      return <Redirect to={'/deals?' + searchParams.toUrlComponent()}/>;
    }

    return (
      <div
        style={{
          ...this.props.style,
          display: 'flex',
          alignItems: 'center',
          position: 'relative',
        }}
      >
        <form onSubmit={event => this.onSubmit(event)}>
          <input
            autoFocus
            type="text"
            placeholder={this.state.hintText}
            value={this.props.searchTerm}
            onChange={event => this.props.onSearchTermChange(event.target.value)}
            style={{
              borderRadius: '5px',
              width: this.state.width,
              height: this.state.height,
              padding: '0 16px',
              fontFamily: 'HelveticaNeue, sans-serif',
              fontSize: '14px',
              color: '#9b9b9b',
            }}
          />
        </form>
        <SearchIcon
          onClick={event => this.onSubmit(event)}
          style={{
            color: '#d8d8d8',
            fontSize: this.state.searchIconSize,
            position: 'absolute',
            right: '4px',
            cursor: 'pointer',
          }}/>
      </div>
    );
  }
}

BaseSearchBar.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
  style: PropTypes.object.isRequired,
  searchTerm: PropTypes.string.isRequired,
  onSearchTermChange: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  searchTerm: state.searchTerm,
});

const mapDispatchToProps = dispatch => ({
  onSearchTermChange: searchTerm => dispatch(updateSearchTerm(searchTerm))
});

export const SearchBar = connect(mapStateToProps, mapDispatchToProps)(withRouter(BaseSearchBar));