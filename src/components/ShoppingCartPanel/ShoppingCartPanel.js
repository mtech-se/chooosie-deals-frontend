import PropTypes from 'prop-types';
import React from 'react';
import { Order } from '../../models/Order/Order';
import { CurrencyUtils } from '../../utils/CurrencyUtils';
import { PanelTitle } from '../PanelTitle/PanelTitle';
import { ShoppingCartList } from '../ShoppingCartList/ShoppingCartList';

export class ShoppingCartPanel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      totalAmount: 0,
    };

    this.calculateTotalAmount = this.calculateTotalAmount.bind(this);
    this.orderUpdated = this.orderUpdated.bind(this);
  }

  componentDidMount() {
    this.calculateTotalAmount();
  }

  calculateTotalAmount() {
    const orderItems = this.props.currentOrder.orderItems;

    let totalAmount = 0;

    orderItems.forEach(function (orderItem) {
      totalAmount = totalAmount + orderItem.spotPrice;
    });

    this.setState({
      totalAmount: CurrencyUtils.toCurrency(totalAmount / 100)
    });
  }

  orderUpdated() {
    this.calculateTotalAmount();
  }

  render() {
    return (
      <div style={{
        fontFamily: 'Roboto, sans-serif',
      }}>
        <PanelTitle
          title={'Shopping Cart'}
        />

        <p style={{
          content: ' ',
          border: '1px solid #eee4e7',
          display: 'block',
          maxWidth: '100%'
        }}>
        </p>

        <ShoppingCartList
          currentOrder={this.props.currentOrder}
          orderUpdated={this.orderUpdated}
        />

        <p style={{
          content: ' ',
          border: '1px solid #eee4e7',
          display: 'block',
          maxWidth: '100%'
        }}>
        </p>

        <div style={{
          display: 'flex',
          color: '##1b2125',
        }}>
          <div style={{
            fontSize: '24px',
          }}>
            Total
          </div>

          <div style={{
            flexGrow: '3',
            display: 'flex',
            alignItems: 'baseline',
            justifyContent: 'flex-end',
          }}>
            <div style={{
              fontSize: '24px',
              fontWeight: 'bold',
            }}>
              {this.state.totalAmount}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ShoppingCartPanel.propTypes = {
  currentOrder: PropTypes.instanceOf(Order),
};