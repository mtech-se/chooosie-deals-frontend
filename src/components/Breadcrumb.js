import * as PropTypes from 'prop-types';
import React from 'react';

export const Breadcrumb = props => (
  <div style={{
    // margin: '0 ' + props.sideMargin,
    padding: '0 128px',
    fontFamily: 'Libre Franklin, sans-serif',
    fontSize: '20px',
    backgroundColor: '#e8e8e8',
    color: '#36173b',
    fontWeight: '700',
    height: '100px',
    display: 'flex',
    alignItems: 'center',
  }}>
    Home &gt; Products &gt; XYZ &gt; Mookata Lunch / Dinner Set for 1 Person
  </div>
);

Breadcrumb.propTypes = {
 sideMargin: PropTypes.string.isRequired,
};