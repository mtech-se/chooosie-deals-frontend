import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { PaymentFactory } from '../../factories/PaymentFactory';
import { Order } from '../../models/Order/Order';
import { MyOrderService } from '../../services/MyOrderService';
import { ActionButton } from '../../views/DealDetailsView/ActionButton';
import { LabelField } from '../LabelField/LabelField';
import { PanelTitle } from '../PanelTitle/PanelTitle';

export class BaseBillingDetailsPanel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      customerFirstName: '',
      customerLastName: '',
      customerAddress: '',
      customerAddressLineOne: '',
      customerAddressLineTwo: '',
      customerAddressCity: '',
      customerAddressCountry: '',
      customerAddressPostalCode: '',
      customerAddressPhoneNumber: '',
      creditCardNumber: '4242424242424242',
      creditCardName: '',
      creditCardExpiryMonth: '10',
      creditCardExpiryYear: '2020',
      creditCardCVV: '123',
      isDisabled: true,
    };
    this.makePayment = this.makePayment.bind(this);
  }

  componentDidMount() {
    if (this.props.currentOrder === undefined || this.props.currentOrder === null) return;

    this.setState({
      customerFirstName: this.props.currentOrder.customer.firstName,
      customerLastName: this.props.currentOrder.customer.lastName,
      customerAddressLineOne: this.props.currentOrder.customer.billingAddressLineOne,
      customerAddressLineTwo: this.props.currentOrder.customer.billingAddressLineTwo,
      customerAddressPostalCode: this.props.currentOrder.customer.billingAddressPostalCode,
      customerAddressPhoneNumber: this.props.currentOrder.customer.billingCellphone,
    });
  }

  isFormComplete() {
    let result = true;
    if (this.state.customerFirstName === '') result = false;
    if (this.state.customerLastName === '') result = false;
    if (this.state.customerAddressLineOne === '') result = false;
    if (this.state.customerAddressLineTwo === '') result = false;
    if (this.state.customerAddressPostalCode === '') result = false;
    if (this.state.customerAddressPhoneNumber === '') result = false;
    if (this.state.creditCardNumber === '') result = false;
    if (this.state.creditCardName === '') result = false;
    if (this.state.creditCardExpiryMonth === '') result = false;
    if (this.state.creditCardExpiryYear === '') result = false;
    if (this.state.creditCardCVV === '') result = false;

    if (result === true) {
      this.setState({
        isDisabled: false
      });
    }
  }

  async makePayment() {
    this.props.setIsMakingNetworkCall(true);

    const requestBody = {
      amount: '2500',
      billing_address: this.state.customerAddressLineOne + ' ' + this.state.customerAddressLineTwo + ' ' + this.state.customerAddressPostalCode,
      currency: 'SGD',
      credit_card: {
        number: this.state.creditCardNumber,
        exp_month: this.state.creditCardExpiryMonth,
        exp_year: this.state.creditCardExpiryYear,
        cvv: this.state.creditCardCVV
      }
    };

    const response = await MyOrderService.makePayment(this.props.currentOrder.id, requestBody);
    if (response.status === 201) {
      const payment = PaymentFactory.createFromJson(response.data);
      this.props.setIsMakingNetworkCall(false);
      this.props.history.push(`/checkout/success?payment_amount=${payment.receiptAmount}&payment_id=${payment.id}`);
    } else throw new Error('Something went wrong - the response was something other than 201');
  }

  render() {
    return (
      <div>
        <PanelTitle
          title={'Billing Details'}
        />

        <div
          style={{
            display: 'flex',
            marginBottom: '16px'
          }}>

          <div
            style={{
              flexGrow: '1',
              marginRight: '8px'
            }}>
            <LabelField
              title={'First Name'}
              type={'text'}
              required={true}
              value={this.state.customerFirstName}
              onChangeHandler={event => {
                this.setState({
                  customerFirstName: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>

          <div
            style={{
              flexGrow: '1'
            }}>
            <LabelField
              title={'Last Name'}
              type={'text'}
              required={true}
              value={this.state.customerLastName}
              onChangeHandler={event => {
                this.setState({
                  customerLastName: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>
        </div>

        <LabelField
          title={'Address (Line One)'}
          type={'text'}
          required={true}
          marginBottom={'16px'}
          value={this.state.customerAddressLineOne}
          onChangeHandler={event => {
            this.setState({
              customerAddressLineOne: event.target.value
            }, () => {
              this.isFormComplete();
            });
          }}
        />

        <LabelField
          title={'Address (Line Two)'}
          type={'text'}
          required={true}
          marginBottom={'16px'}
          value={this.state.customerAddressLineTwo}
          onChangeHandler={event => {
            this.setState({
              customerAddressLineTwo: event.target.value
            }, () => {
              this.isFormComplete();
            });
          }}
        />

        <div
          style={{
            display: 'flex',
            marginBottom: '32px'
          }}>

          <div
            style={{
              flexGrow: '1',
              marginRight: '8px'
            }}>
            <LabelField
              title={'Postal Code'}
              type={'text'}
              required={true}
              value={this.state.customerAddressPostalCode}
              onChangeHandler={event => {
                this.setState({
                  customerAddressPostalCode: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>

          <div
            style={{
              flexGrow: '1'
            }}>
            <LabelField
              title={'Phone'}
              type={'text'}
              required={true}
              value={this.state.customerAddressPhoneNumber}
              onChangeHandler={event => {
                this.setState({
                  customerAddressPhoneNumber: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>
        </div>

        <PanelTitle
          title={'Credit Card Details'}
        />

        <div
          style={{
            display: 'flex',
            marginBottom: '16px'
          }}>

          <div
            style={{
              flexGrow: '1',
              marginRight: '8px'
            }}>

            <LabelField
              title={'Name on card'}
              type={'text'}
              required={true}
              value={this.state.creditCardName}
              onChangeHandler={event => {
                this.setState({
                  creditCardName: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>

          <div
            style={{
              flexGrow: '1',
            }}>
            <LabelField
              title={'Credit Card Number'}
              type={'number'}
              required={true}
              value={this.state.creditCardNumber}
              onChangeHandler={event => {
                this.setState({
                  creditCardNumber: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>
        </div>

        <div
          style={{
            display: 'flex',
            marginBottom: '16px'
          }}>

          <div
            style={{
              flexGrow: '1',
              marginRight: '8px'
            }}>

            <LabelField
              title={'Expiry Month'}
              type={'number'}
              required={true}
              value={this.state.creditCardExpiryMonth}
              onChangeHandler={event => {
                this.setState({
                  creditCardExpiryMonth: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>

          <div
            style={{
              flexGrow: '1',
              marginRight: '8px'
            }}>

            <LabelField
              title={'Expiry Year'}
              type={'number'}
              required={true}
              value={this.state.creditCardExpiryYear}
              onChangeHandler={event => {
                this.setState({
                  creditCardExpiryYear: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>

          <div
            style={{
              flexGrow: '1'
            }}>
            <LabelField
              title={'CVV/CVC'}
              type={'number'}
              required={true}
              value={this.state.creditCardCVV}
              onChangeHandler={event => {
                this.setState({
                  creditCardCVV: event.target.value
                }, () => {
                  this.isFormComplete();
                });
              }}
            />
          </div>
        </div>

        <div style={{
          marginTop: '24px',
        }}>
          <ActionButton
            label={'Make Payment'}
            width={'100%'}
            color={'#eeeeee'}
            backgroundColor={'#601e59'}
            fontSize={'18px'}
            disabled={this.state.isDisabled}
            onClick={this.makePayment}
          />
        </div>
      </div>
    );
  }
}

BaseBillingDetailsPanel.propTypes = {
  currentOrder: PropTypes.instanceOf(Order),
  setIsMakingNetworkCall: PropTypes.func.isRequired,
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const BillingDetailsPanel = withRouter(BaseBillingDetailsPanel);

BillingDetailsPanel.propTypes = {
  currentOrder: PropTypes.instanceOf(Order),
  setIsMakingNetworkCall: PropTypes.func.isRequired,
};