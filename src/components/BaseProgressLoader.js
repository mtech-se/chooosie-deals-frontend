import { CircularProgress } from '@material-ui/core/es/index';
import * as PropTypes from 'prop-types';
import React from 'react';
import { ColorConstants } from '../constants/ColorConstants';

export const BaseProgressLoader = props => (
  <div style={{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    ...props.style
  }}>
    <CircularProgress
      size={50}
      style={{
        color: ColorConstants.primaryPurple,
      }}
    />
    <div style={{
      marginTop: '16px',
      fontFamily: 'Roboto, sans serif',
      fontWeight: '300',
    }}>{props.loadingMessage}</div>
  </div>
);

BaseProgressLoader.propTypes = {
  loadingMessage: PropTypes.string,
  style: PropTypes.object,
};

BaseProgressLoader.defaultProps = {
  loadingMessage: 'Loading...',
};