import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Deal } from '../models/Deal';
import { DealCardLandscapeSmall } from './DealCard/DealCardLandscapeSmall';
import { SectionTitle } from './SectionTitle';

export class DotmDealsPanel extends Component {
  render() {
    if (this.props.deals === undefined) return 'Fetching...';

    const dealCardWideElements = [];
    const maxElements = this.props.deals.length < 10 ? this.props.deals.length : 10;

    for (let i = 0; i < maxElements; i++) {
      const deal = this.props.deals[i];

      dealCardWideElements.push((
        <DealCardLandscapeSmall
          key={deal.id}
          bottomMargin={'8px'}
          deal={deal}
        />
      ));
    }

    return (
      <div style={{
        marginLeft: '32px',
      }}>
        <div style={{ marginBottom: '16px' }}>
          <SectionTitle
            title={'Deals of The Month'}
          />
        </div>
        <div>
          {dealCardWideElements}
        </div>
      </div>
    );
  }
}

DotmDealsPanel.propTypes = {
  deals: PropTypes.arrayOf(PropTypes.instanceOf(Deal)),
};