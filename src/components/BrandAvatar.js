import { Avatar } from '@material-ui/core/es/index';
import withStyles from '@material-ui/core/es/styles/withStyles';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';

const styles = {};

class UnstyledBrandAvatar extends Component {
  render() {
    const { classes } = this.props;

    return (
      <Avatar
        alt="Brand image"
        src={this.props.image}
        className={classes.avatar}
        style={{
          width: this.props.size,
          height: this.props.size,
          backgroundColor: 'white',
        }}
      />
    );
  }
}

UnstyledBrandAvatar.propTypes = {
  image: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
};

export const BrandAvatar = withStyles(styles)(UnstyledBrandAvatar);

BrandAvatar.propTypes = {
  image: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
};
