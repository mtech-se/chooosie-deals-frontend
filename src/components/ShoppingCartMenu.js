import { IconButton } from '@material-ui/core';
import * as PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import shoppingCartIcon from '../assets/images/shopping-cart-icon.png';
import { Customer } from '../models/Customer';

const BaseShoppingCartMenu = props => {
  if (props.customer === undefined) return null;

  return (
    <IconButton onClick={() => props.history.push('/checkout')}>
      <img
        src={shoppingCartIcon}
        alt="Shopping Cart Menu"
        style={{
          width: '32px',
          height: '32px',
        }}
      />
    </IconButton>
  );
};

BaseShoppingCartMenu.propTypes = {
  customer: PropTypes.instanceOf(Customer).isRequired,
  history: ReactRouterPropTypes.history.isRequired,
};

const mapStateToProps = state => ({
  customer: state.customer,
});

export const ShoppingCartMenu = connect(mapStateToProps)(withRouter(BaseShoppingCartMenu));