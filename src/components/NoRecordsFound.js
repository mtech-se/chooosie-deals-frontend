import * as PropTypes from 'prop-types';
import React from 'react';
import { StyleConstants } from '../constants/StyleConstants';

export const NoRecordsFound = props => {
  return (
    <div
      style={{
        textAlign: 'center',
        fontFamily: StyleConstants.fontFamily,
      }}>
      No {props.itemLabel} found
    </div>
  );
};

NoRecordsFound.propTypes = {
  itemLabel: PropTypes.string.isRequired,
};