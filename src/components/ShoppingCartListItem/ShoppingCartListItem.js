import IconButton from '@material-ui/core/es/IconButton/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import * as PropTypes from 'prop-types';
import React from 'react';
import { OrderItem } from '../../models/OrderItem';
import { CurrencyUtils } from '../../utils/CurrencyUtils';

export class ShoppingCartListItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      orderItem: undefined
    };

    this.removeOrderItem = this.removeOrderItem.bind(this);
  }

  componentDidMount() {
    const orderItem = this.props.orderItem;

    this.setState({
      orderItem
    });
  }

  removeOrderItem() {
    this.props.removeOrderItem(this.state.orderItem);
  }

  render() {
    if (this.state.orderItem) {
      return (
        <div style={{
          marginBottom: '8px',
        }}>
          <div style={{
            display: 'flex',
            alignItems: 'center',
          }}>
            <div style={{
              marginRight: '16px'
            }}>
              <img style={{
                width: '88px',
                height: '88px'
              }}
                   src={this.state.orderItem.deal.getMainPhotoUrl()}
                   alt="Logo"/>
            </div>

            <div style={{
              fontSize: '16px',
              color: '#788995',
              marginRight: '16px'
            }}>
              <div style={{
                fontWeight: 'bold',
                marginBottom: '8px'
              }}>{this.state.orderItem.deal.title}
              </div>

              <div style={{
                fontWeight: 'bolder',
                marginBottom: '8px'
              }}>
                {CurrencyUtils.toCurrency(this.state.orderItem.spotPrice / 100)}
              </div>
            </div>

            <div style={{
              flexGrow: '3',
              display: 'flex',
              alignItems: 'baseline',
              justifyContent: 'flex-end',
              color: '#788995',
            }}>
              <IconButton
                onClick={this.removeOrderItem}
              >
                <DeleteIcon/>
              </IconButton>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div/>
    );
  }
}

ShoppingCartListItem.propTypes = {
  orderItem: PropTypes.instanceOf(OrderItem),
  removeOrderItem: PropTypes.func
};