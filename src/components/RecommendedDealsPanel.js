import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Deal } from '../models/Deal';
import { DealCardLandscapeSmall } from './DealCard/DealCardLandscapeSmall';
import { SectionTitle } from './SectionTitle';

class BaseRecommendedDealsPanel extends Component {
  render() {
    if (this.props.deals === undefined) return 'Fetching...';

    const dealCardWideElements = [];
    const maxElements = this.props.deals.length < 10 ? this.props.deals.length : 10;

    for (let i = 0; i < maxElements; i++) {
      const deal = this.props.deals[i];

      dealCardWideElements.push((
        <DealCardLandscapeSmall
          key={deal.id}
          bottomMargin={'8px'}
          deal={deal}
        />
      ));
    }


    return (
      <div style={{
        ...this.props.style
      }}>
        <div style={{ marginBottom: '16px' }}>
          <SectionTitle
            title={'Recommended Deals'}
          />
        </div>
        <div>
          {dealCardWideElements}
        </div>
      </div>
    );
  }
}

BaseRecommendedDealsPanel.propTypes = {
  deals: PropTypes.arrayOf(PropTypes.instanceOf(Deal)),
  style: PropTypes.object,
};

export const RecommendedDealsPanel = withRouter(BaseRecommendedDealsPanel);