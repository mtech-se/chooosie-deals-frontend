import { Grid } from '@material-ui/core/es/index';
import React, { Component } from 'react';
import { CategoryFactory } from '../../factories/CategoryFactory';
import { CategoryService } from '../../services/CategoryService';
import { BaseProgressLoader } from '../BaseProgressLoader';
import { NoRecordsFound } from '../NoRecordsFound';
import { SectionTitle } from '../SectionTitle';
import { DealCategory } from './DealCategory';

export class CategoryDeals extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasFetchedData: false,
      categories: undefined,
      numItemsToShow: 0
    };

    this.onResize = this.onResize.bind(this);
  }

  async componentDidMount() {
    window.addEventListener('resize', this.onResize);
    this.onResize();

    const response = await CategoryService.getCategories();
    const categories = CategoryFactory.createFromJsonArray(response.data.content);
    this.setState({
      hasFetchedData: true,
      categories,
    });
  }

  onResize() {
    if (window.innerWidth < 960) {
      this.setState({
        numItemsToShow: 1
      });
    } else {
      this.setState({
        numItemsToShow: 3
      });
    }
  }

  render() {
    if (this.state.hasFetchedData === false) return <BaseProgressLoader/>;
    if (this.state.categories.length === 0) return <NoRecordsFound itemLabel={'categories'}/>;

    const items = [];
    for (let i = 0; i < 3; i++) {
      const category = this.state.categories[i];
      const marginRight = i === 3 - 1 ? '0' : '16px';

      items.push(
        <div key={category.id}
             style={{ marginRight }}>
          <DealCategory
            category={category}
          />
        </div>
      );
    }

    return (
      <Grid container
            justify={'center'}>
        <Grid item
              xs={12}
              style={{ display: 'flex', justifyContent: 'center' }}>

          <div>
            <div style={{ marginBottom: '16px' }}>
              <SectionTitle title={'You might be interested in'}/>
            </div>
            <div style={{ display: 'flex' }}>
              {items}
            </div>
          </div>

        </Grid>
      </Grid>
    );
  }
}