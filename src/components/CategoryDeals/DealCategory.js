import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import placeholderImage from '../../assets/images/224x151.png';
import { ColorConstants } from '../../constants/ColorConstants';
import { Category } from '../../models/Category';

export class DealCategory extends React.Component {
  render() {
    const categoryPhotoUrl = this.props.category.entityPhotos === undefined ? placeholderImage : this.props.category.getMainPhotoUrl();
    const squareDimension = '302px';

    return (
      <Link to={'deals?categoryId=' + this.props.category.id}>
        <div style={{
          position: 'relative',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}>

          <img
            src={categoryPhotoUrl}
            alt="Deal category"
            style={{
              width: squareDimension,
              height: squareDimension,
              objectFit: 'cover',
              display: 'block',
              borderRadius: '8px',
            }}
          />

          <div style={{
            width: squareDimension,
            height: squareDimension,
            backgroundColor: ColorConstants.translucentBlack,
            position: 'absolute',
            borderRadius: '8px',
          }}/>

          <div
            style={{
              fontFamily: 'Libre Franklin',
              fontSize: '24px',
              fontWeight: '700',
              color: '#ffffff',
              textTransform: 'uppercase',
              position: 'absolute',
              width: '100%',
              textAlign: 'center',
              padding: '0 16px',
            }}>{this.props.category.name}
          </div>

        </div>
      </Link>
    );
  }
}

DealCategory.propTypes = {
  category: PropTypes.instanceOf(Category).isRequired,
};