import { Chip } from '@material-ui/core/es/index';
import withStyles from '@material-ui/core/es/styles/withStyles';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';

const styles = {};

class UnstyledDiscountChip extends Component {
  render() {
    const { classes } = this.props;

    return (
      <Chip
        label={this.props.label}
        className={classes.chip}
        style={{
          backgroundColor: '#0da297',
          color: '#ffffff',
          fontWeight: '700',
          fontFamily: 'Libre Franklin',
          fontSize: '12px',
        }}
      />
    );
  }
}

UnstyledDiscountChip.propTypes = {
  classes: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
};

export const DiscountChip = withStyles(styles)(UnstyledDiscountChip);

DiscountChip.propTypes = {
  label: PropTypes.string.isRequired,
};