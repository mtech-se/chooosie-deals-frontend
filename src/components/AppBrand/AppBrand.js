import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/deals-logo.png';
import { ColorConstants } from '../../constants/ColorConstants';

export const AppBrand = props => {
  return (
    <div style={{
      backgroundColor: ColorConstants.primaryPurple,
      textAlign: 'center',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      ...props.style,
    }}>
      <Link to="/">
        <img
          src={logo}
          alt="Deals Logo"
          style={{
            height: props.imageHeight,
            display: 'block',
            margin: props.imageMargin,
          }}
        />
      </Link>
    </div>
  );
};

AppBrand.propTypes = {
  imageHeight: PropTypes.string,
  imageMargin: PropTypes.string,
  style: PropTypes.object,
};

AppBrand.defaultProps = {
  imageHeight: '48px',
  imageMargin: '0',
};