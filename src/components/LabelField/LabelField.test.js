import { shallow } from 'enzyme';
import React from 'react';
import { LabelField } from './LabelField';

describe('LabelField', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<LabelField title={'test'}
                        type={'test'}/>);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});