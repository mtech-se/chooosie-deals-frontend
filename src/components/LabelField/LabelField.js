import TextField from '@material-ui/core/TextField';
import * as PropTypes from 'prop-types';
import React from 'react';

export const LabelField = props => {
  return (
    <div style={{
      marginBottom: props.marginBottom !== undefined ? props.marginBottom : 'initial'
    }}>
      <TextField
        label={props.title}
        type={props.type}
        value={props.value}
        onChange={props.onChangeHandler}
        multiline={props.multiline}
        rows="3"
        fullWidth
        disabled={props.isDisabled}
        required={props.required}
        margin="dense"
        variant="outlined"
        inputProps={{
          maxLength: 255,
        }}
      />
    </div>
  );
};

LabelField.propTypes = {
  marginBottom: PropTypes.string,
  title: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChangeHandler: PropTypes.func,
  required: PropTypes.bool,
  multiline: PropTypes.bool,
  isDisabled: PropTypes.bool
};