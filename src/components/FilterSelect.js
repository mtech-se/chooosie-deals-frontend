import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import Select from 'react-select';
import { ColorConstants } from '../constants/ColorConstants';

export class FilterSelect extends Component {
  static getOptionBackgroundColor(optionState) {
    let backgroundColor = null;
    if (optionState.isSelected) backgroundColor = ColorConstants.primaryPurple;
    return backgroundColor;
  }

  static getOptionFontColor(optionState) {
    let backgroundColor = null;
    if (optionState.isSelected) backgroundColor = ColorConstants.white;
    return backgroundColor;
  }

  render() {
    const colourStyles = {
      option: (styles, state) => ({
          ...styles,
          backgroundColor: FilterSelect.getOptionBackgroundColor(state),
          color: FilterSelect.getOptionFontColor(state),
        }
      )
    };

    return (
      <div style={{
        ...this.props.style,
        width: '280px',
      }}>
        <div style={{
          fontSize: '12px',
          fontFamily: 'Libre Franklin, sans-serif',
          fontWeight: '300',
          marginBottom: '4px',
        }}>{this.props.label}
        </div>
        <Select
          value={this.props.value}
          options={this.props.options}
          onChange={this.props.onChange}
          style={{
            backgroundColor: 'red',
            color: 'red',
          }}
          styles={colourStyles}
          isDisabled={this.props.isDisabled}
          isClearable
        />
      </div>
    );
  }
}

const optionShape = {
  value: PropTypes.number,
  label: PropTypes.string,
};

FilterSelect.propTypes = {
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape(optionShape)).isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.shape(optionShape),
  isDisabled: PropTypes.bool,
  style: PropTypes.object,
};