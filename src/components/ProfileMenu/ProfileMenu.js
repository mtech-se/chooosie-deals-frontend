import { IconButton, Menu, MenuItem } from '@material-ui/core';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import profileIcon from '../../assets/images/profile-icon.png';
import { Customer } from '../../models/Customer';

class BaseProfileMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorElement: null,
    };
  }

  handleMenuClick = event => {
    this.setState({ anchorElement: event.currentTarget });
  };

  handleMenuItemClick = linkType => {
    switch (linkType) {
      case 'SIGN_IN':
        this.props.history.push('/login');
        break;
      case 'MY_ACCOUNT':
        this.props.history.push('/my-account');
        break;
      case 'LOGOUT':
        this.props.history.push('/logout');
        break;
      default:
        break;
    }

    this.setState({ anchorElement: null });
  };

  render() {
    const menuItemsIfAuthenticated = [
      <MenuItem
        key={1}
        onClick={() => this.handleMenuItemClick('MY_ACCOUNT')}>My Account
      </MenuItem>,
      <MenuItem
        key={2}
        onClick={() => this.handleMenuItemClick('LOGOUT')}>Logout
      </MenuItem>
    ];

    const menuItemsIfGuest = (
      <MenuItem onClick={() => this.handleMenuItemClick('SIGN_IN')}>Sign in</MenuItem>
    );

    return (
      <div style={{
        display: 'flex',
        alignItems: 'center',
      }}>
        <IconButton
          aria-owns={this.state.anchorElement ? 'simple-menu' : undefined}
          aria-haspopup="true"
          onClick={this.handleMenuClick}
        >
          <img
            src={profileIcon}
            alt="Profile Menu"
            style={{
              width: '32px',
              height: '32px',
            }}
          />
        </IconButton>

        <div style={{
          marginLeft: '4px',
        }}>{this.props.customer === undefined ? 'Guest' : this.props.customer.firstName}
        </div>

        <Menu
          id="simple-menu"
          anchorEl={this.state.anchorElement}
          open={Boolean(this.state.anchorElement)}
          onClose={this.handleMenuItemClick}
        >
          {this.props.customer === undefined ? menuItemsIfGuest : menuItemsIfAuthenticated}
        </Menu>
      </div>
    );
  }
}

BaseProfileMenu.propTypes = {
  customer: PropTypes.instanceOf(Customer),
  history: ReactRouterPropTypes.history.isRequired,
};

const mapStateToProps = state => ({
  customer: state.customer,
});

export const ProfileMenu = connect(mapStateToProps)(withRouter(BaseProfileMenu));