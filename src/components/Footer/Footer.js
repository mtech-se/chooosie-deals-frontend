import React from 'react';
import { FooterColumn } from './FooterColumn';

export const Footer = () => (
  <div style={{
    display: 'flex',
    justifyContent: 'space-evenly',
    backgroundColor: '#b21b76',
    padding: '48px 152px',
  }}>

    <FooterColumn
      title={'About us'}
      width={'200px'}
      content={(
        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</div>
      )}
    />

    <FooterColumn
      title={'Services'}
      width={'200px'}
      content={(
        <div>
          <div>Listing</div>
          <div>Deals</div>
          <div>Market with us</div>
          <div>Partners</div>
        </div>
      )}
    />

    <FooterColumn
      title={'Features'}
      width={'200px'}
      content={(
        <div>
          <div>FAQ</div>
          <div>Sitemap</div>
          <div>Terms</div>
          <div>Privacy</div>
          <div>For merchants</div>
        </div>
      )}
    />

    <FooterColumn
      title={'Brands'}
      width={'200px'}
      content={(
        <div>
          <div>halalfoodhunt.com</div>
          <div>Sifted</div>
          <div>Friends</div>
          <div>Deals</div>
        </div>
      )}
    />

  </div>
);