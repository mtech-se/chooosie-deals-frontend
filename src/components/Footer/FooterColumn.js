import * as PropTypes from 'prop-types';
import React from 'react';

export const FooterColumn = props => (
  <div style={{
    maxWidth: props.width,
  }}>
    <div style={{
      fontFamily: 'Libre Franklin',
      color: 'white',
      textTransform: 'uppercase',
      fontSize: '18px',
      fontWeight: '700',
      marginBottom: '16px',
    }}>{props.title}</div>
    <div style={{
      color: 'white',
      fontFamily: 'Libre Franklin',
      fontWeight: '100',
      fontSize: '14px',
      lineHeight: '30px',
    }}>{props.content}</div>
  </div>
);

FooterColumn.propTypes = {
  title: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  content: PropTypes.element.isRequired,
};