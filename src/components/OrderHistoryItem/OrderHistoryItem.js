import Dialog from '@material-ui/core/es/Dialog/Dialog';
import DialogTitle from '@material-ui/core/es/DialogTitle/DialogTitle';
import IconButton from '@material-ui/core/es/IconButton/IconButton';
import RedeemIcon from '@material-ui/icons/Redeem';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Order } from '../../models/Order/Order';
import { OrderItem } from '../../models/OrderItem';
import { MyOrderService } from '../../services/MyOrderService';
import * as DateUtils from '../../utils/DateUtils';
import { ActionButton } from '../../views/DealDetailsView/ActionButton';

export class OrderHistoryItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      orderPaymentDate: undefined,
      isRedemptionDateHidden: true,
      isExpiryDateHidden: false,
      orderItemExpiryDate: undefined,
      orderItemRedemptionDate: undefined,
      isDialogVisible: false,
      isRedeemIconDisabled: false,
      isRedeemButtonDisabled: false,

    };
    this.handleOpenDialog = this.handleOpenDialog.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.redeemOrderItem = this.redeemOrderItem.bind(this);
  }

  componentDidMount() {
    this.getOrderPaymentDate();
    this.hasOrderItemBeenRedeemed();
  }

  getOrderPaymentDate() {
    const order = this.props.order;

    if (order.payments.length <= 2) {
      this.setState({
        orderPaymentDate: DateUtils.toFriendlyDateString(order.payments[order.payments.length - 1].updatedAt)
      });
    }
  }

  hasOrderItemBeenRedeemed() {
    const orderItem = this.props.orderItem;
    let isRedemptionDateHidden = undefined;
    let isExpiryDateHidden = undefined;
    let isRedeemIconDisabled = undefined;

    if (orderItem.redeemedAt) {
      isExpiryDateHidden = true;
      isRedeemIconDisabled = true;
    }

    if (!orderItem.redeemedAt) {
      isRedemptionDateHidden = true;
    }

    this.setState({
      isExpiryDateHidden,
      isRedeemIconDisabled,
      isRedemptionDateHidden,
      orderItemExpiryDate: DateUtils.toFriendlyDateString(orderItem.deal.redemptionExpiryDate),
      orderItemRedemptionDate: DateUtils.toFriendlyDateString(orderItem.redeemedAt)
    });
  }

  redeemOrderItem() {
    this.setState({
      isRedeemButtonDisabled: true,
    }, async () => {
      const responseRedeemOrder = await MyOrderService.redeemOrder(this.props.orderItem.id);

      if (responseRedeemOrder.status === 200) {
        this.handleCloseDialog();
        this.props.onOrderItemRedeemed();
      }
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  handleOpenDialog = () => {
    this.setState({ isDialogVisible: true });
  };

  handleCloseDialog = () => {
    this.setState({ isDialogVisible: false });
  };

  render() {
    return (
      <div>
        <p style={{
          content: ' ',
          border: '1px solid #eee4e7',
          display: 'block',
          maxWidth: '100%'
        }}>
        </p>

        <div style={{
          fontSize: '14px',
          color: '#788995',
          marginBottom: '16px'
        }}>
          Order Number: {this.props.order.id}
        </div>

        <div style={{
          marginBottom: '8px',
        }}>
          <div style={{
            display: 'flex',
            alignItems: 'center',
          }}>
            <div style={{
              flexGrow: '1',
              padding: '10px 10px'
            }}>
              <img style={{
                width: '88px',
                height: '88px',
                borderRadius: '4%'
              }}
                   src={this.props.orderItem.deal.getMainPhotoUrl()}
                   alt="Logo"/>
            </div>

            <div style={{
              flexGrow: '1',
              fontSize: '16px',
              marginRight: '16px'
            }}>
              <div>
                {this.props.orderItem.deal.title}
              </div>
            </div>

            <div
              style={{
                flexGrow: '1',
                display: 'flex',
                justifyContent: 'flex-end',
              }}>
              <div style={{
                flexGrow: '1',
                fontSize: '16px',
                marginRight: '16px'
              }}>
                <div style={{
                  textAlign: 'center'
                }}>
                  Bought on
                  <br/>
                  {this.state.orderPaymentDate}
                </div>
              </div>

              <div
                hidden={this.state.isRedemptionDateHidden}
                style={{
                  flexGrow: '1',
                  fontSize: '16px',
                  marginRight: '16px'
                }}>
                <div style={{
                  textAlign: 'center'
                }}>
                  Redeemed on
                  <br/>
                  {this.state.orderItemRedemptionDate}
                </div>
              </div>

              <div
                hidden={this.state.isExpiryDateHidden}
                style={{
                  flexGrow: '1',
                  fontSize: '16px',
                  marginRight: '16px'
                }}>
                <div style={{
                  textAlign: 'center'
                }}>
                  Expires on
                  <br/>
                  {this.state.orderItemExpiryDate}
                </div>
              </div>

              <div
                style={{
                  flexGrow: '1',
                  color: '#788995',
                  padding: '0px 10px',
                }}
              >
                <IconButton
                  disabled={this.state.isRedeemIconDisabled}
                  onClick={this.handleOpenDialog}
                >
                  <RedeemIcon/>
                </IconButton>
              </div>
            </div>
          </div>
        </div>

        <Dialog
          open={this.state.isDialogVisible}
          onClose={this.handleCloseDialog}
        >
          <div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                width: '500px',
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: '8px'
              }}>
              <DialogTitle>{this.props.orderItem.deal.title}</DialogTitle>
              Do you want to redeem this deal now?
            </div>

            <div style={{
              padding: '32px 24px'
            }}>
              <ActionButton
                label={'Redeem Now'}
                width={'100%'}
                color={'#eeeeee'}
                backgroundColor={'#601e59'}
                fontSize={'18px'}
                disabled={this.state.isRedeemButtonDisabled}
                onClick={this.redeemOrderItem}
              />
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

OrderHistoryItem.propTypes = {
  order: PropTypes.instanceOf(Order),
  orderItem: PropTypes.instanceOf(OrderItem),
  onOrderItemRedeemed: PropTypes.func.isRequired,
};