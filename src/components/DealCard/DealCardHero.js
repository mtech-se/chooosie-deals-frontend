import withStyles from '@material-ui/core/es/styles/withStyles';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import placeholderHeroImage from '../../assets/images/1000x500.png';
import { DimensionConstants } from '../../constants/DimensionConstants';
import { Deal } from '../../models/Deal';
import { BrandAvatar } from '../BrandAvatar';
import { DiscountChip } from '../DiscountChip';

const styles = {};

class UnstyledDealCardHero extends Component {
  render() {
    let dealPhotoUrl = placeholderHeroImage;
    if (this.props.deal.entityPhotos.length !== 0 && this.props.deal.getMainPhotoUrl() !== undefined) dealPhotoUrl = this.props.deal.getMainPhotoUrl();

    let brandPhotoUrl = placeholderHeroImage;
    if (this.props.deal.outlet.brand.entityPhotos.length !== 0) brandPhotoUrl = this.props.deal.outlet.brand.entityPhotos[0].getPhotoUrl();

    return (
      <div style={{
        position: 'relative',
        borderRadius: '4px',
      }}>

        <Link to={'deals/' + this.props.deal.id}>
          <img
            src={dealPhotoUrl}
            alt={`Deal ID: ${this.props.deal.id}`}
            style={{
              width: DimensionConstants.dealHeroImageWidth,
              height: DimensionConstants.dealHeroImageHeight,
              display: 'block',
              zIndex: '1',
              borderRadius: '4px',
            }}
          />
        </Link>

        <div style={{
          position: 'absolute',
          top: '16px',
          right: '16px',
        }}>
          <DiscountChip label={this.props.deal.getDiscountPercentage() + ' off'}/>
        </div>

        <div style={{
          position: 'absolute',
          bottom: '40px',
          left: '40px',
          display: 'flex',
          alignItems: 'center',
        }}>
          <BrandAvatar
            size={'40px'}
            image={brandPhotoUrl}
          />
          <div style={{ marginLeft: '16px' }}>
            <div style={{
              color: '#ffffff',
              fontFamily: 'Libre Franklin, sans-serif',
              fontSize: '24px',
              fontWeight: '900',
            }}>Deal of The Month
            </div>
            <div style={{
              marginTop: '4px',
              color: '#ffffff',
              fontFamily: 'Libre Franklin, sans-serif',
              fontSize: '14px',
              fontWeight: '500',
            }}>{this.props.deal.title}
            </div>
          </div>
        </div>

      </div>
    );
  }
}

UnstyledDealCardHero.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
};

export const DealCardHero = withStyles(styles)(UnstyledDealCardHero);

DealCardHero.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
};