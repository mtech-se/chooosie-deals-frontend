import withStyles from '@material-ui/core/es/styles/withStyles';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import placeholderHeroImage from '../../assets/images/1000x500.png';
import placeholderImage from '../../assets/images/224x151.png';
import { ApiConstants } from '../../constants/ApiConstants';
import { DimensionConstants } from '../../constants/DimensionConstants';
import { Deal } from '../../models/Deal';
import { BrandAvatar } from '../BrandAvatar';
import { DiscountChip } from '../DiscountChip';

const styles = {};

class UnstyledDealCardPortrait extends Component {
  render() {
    let dealPhotoUrl = placeholderImage;
    if (this.props.deal.entityPhotos.length !== 0) {
      const firstDealPhoto = this.props.deal.entityPhotos[0];
      const firstPhoto = firstDealPhoto.photo;

      const photoBaseUrl = ApiConstants.BASE_URL + '/photos/';
      dealPhotoUrl = photoBaseUrl + firstPhoto.id;
    }

    let brandPhotoUrl = placeholderHeroImage;
    if (this.props.deal.outlet.brand.entityPhotos.length !== 0) brandPhotoUrl = this.props.deal.outlet.brand.entityPhotos[0].getPhotoUrl();

    const className = this.props.shouldAnimate ? 'DealCard__animate' : 'DealCardPortrait';

    return (
      <div
        style={{
          marginRight: this.props.rightMargin,
          width: '224px',
          backgroundColor: '#eeeeee',
          height: '300px',
          position: 'relative',
          borderRadius: '4px',
        }}
        className={className}
      >
        <Link to={'deals/' + this.props.deal.id}
              style={{
                textDecoration: 'none',
              }}>
          <img
            src={dealPhotoUrl}
            alt="Deal ID 1"
            style={{
              width: DimensionConstants.dealCardImageWidth,
              height: DimensionConstants.dealCardImageHeight,
              borderRadius: '4px 4px 0 0',
            }}
          />

          <div style={{
            position: 'absolute',
            top: '111px',
            left: '10px',
          }}>
            <BrandAvatar
              image={brandPhotoUrl}
              size={'29px'}
            />
          </div>

          <div style={{
            position: 'absolute',
            top: '12px',
            right: '8px',
          }}>
            <DiscountChip label={this.props.deal.getDiscountPercentage() + ' off'}/>
          </div>

          <div style={{ padding: '16px' }}>
            <div style={{
              color: '#36173b',
              fontFamily: 'Libre Franklin, sans-serif',
              fontSize: '14px',
              fontWeight: '900',
            }}>
              {this.props.deal.title}
            </div>
            <div style={{
              color: '#4a4a4a',
              fontFamily: 'Libre Franklin, sans-serif',
              fontSize: '12px',
              fontWeight: 'light',
              marginTop: '7px',
            }}>
              {this.props.deal.outlet.brand.name}
            </div>
            <div style={{
              fontFamily: 'Libre Franklin, sans-serif',
              marginTop: '9px',
              float: 'right',
            }}>
            <span style={{
              fontSize: '14px',
              fontWeight: '500',
              textDecoration: 'line-through',
              color: '#4a4a4a',
              marginRight: '4px',
            }}>{this.props.deal.getOriginalPrice()}</span>
              <span style={{
                fontSize: '16px',
                fontWeight: '700',
                color: '#417505',
              }}>{this.props.deal.getDiscountedPrice()}</span>
            </div>
          </div>
        </Link>
      </div>
    );
  }
}

UnstyledDealCardPortrait.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
  rightMargin: PropTypes.string.isRequired,
  shouldAnimate: PropTypes.bool,
};

export const DealCardPortrait = withStyles(styles)(UnstyledDealCardPortrait);

DealCardPortrait.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
  rightMargin: PropTypes.string.isRequired,
  shouldAnimate: PropTypes.bool,
};
