import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import placeholderImage from '../../assets/images/224x151.png';
import { ColorConstants } from '../../constants/ColorConstants';
import { DimensionConstants } from '../../constants/DimensionConstants';
import { Deal } from '../../models/Deal';
import { StringUtils } from '../../utils/StringUtils';

class BaseDealCardLandscapeSmall extends Component {
  render() {
    let dealPhotoUrl = placeholderImage;
    if (this.props.deal.entityPhotos.length !== 0) dealPhotoUrl = this.props.deal.entityPhotos[0].getPhotoUrl();

    return (
      <Link
        style={{ textDecoration: 'none', }}
        to={'/deals/' + this.props.deal.id}
      >
        <div style={{
          marginBottom: this.props.bottomMargin,
          backgroundColor: ColorConstants.neutralOffWhite,
          position: 'relative',

          // width: DimensionConstants.dealCardImageWidth,
          height: DimensionConstants.dealCardLandscapeSmallImageHeight,
        }}>

          <div style={{
            display: 'flex'
          }}>
            <img
              src={dealPhotoUrl}
              alt={'Deal ID ' + this.props.deal.id}
              style={{
                width: DimensionConstants.dealCardLandscapeSmallImageWidth,
                minWidth: DimensionConstants.dealCardLandscapeSmallImageWidth,
                height: DimensionConstants.dealCardLandscapeSmallImageHeight,
                objectFit: 'cover',
                // display: 'block',
              }}
            />

            <div style={{
              display: 'flex',
              padding: '4px',
            }}>
              <div style={{
                fontFamily: 'Libre Franklin',
                width: '100%',
                height: '100%',
                marginLeft: '8px',
                marginTop: '4px',
              }}>

                <div style={{
                  fontSize: '14px',
                  fontWeight: '700',
                  color: '#36173b',
                }}>{StringUtils.ellipsify(this.props.deal.title, 40)}
                </div>

                <div style={{
                  marginTop: '16px',
                }}>

                  <span style={{
                    fontSize: '10px',
                    fontWeight: '500',
                    textDecoration: 'line-through',
                    color: '#4a4a4a',
                    marginRight: '4px',
                  }}>{this.props.deal.getOriginalPrice()}</span>
                  <span style={{
                    fontSize: '12px',
                    fontWeight: '700',
                    color: '#417505',
                  }}>{this.props.deal.getDiscountedPrice()}</span>

                </div>
              </div>
            </div>
          </div>
        </div>
      </Link>
    );
  }
}

BaseDealCardLandscapeSmall.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
  bottomMargin: PropTypes.string.isRequired,
  history: ReactRouterPropTypes.history.isRequired,
};

export const DealCardLandscapeSmall = withRouter(BaseDealCardLandscapeSmall);
