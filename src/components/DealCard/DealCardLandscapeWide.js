import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import placeholderImage from '../../assets/images/224x151.png';
import placeholderBrandImage from '../../assets/images/48x48.png';
import { ColorConstants } from '../../constants/ColorConstants';
import { DimensionConstants } from '../../constants/DimensionConstants';
import { Deal } from '../../models/Deal';
import { BrandAvatar } from '../BrandAvatar';

export class DealCardLandscapeWide extends Component {
  render() {
    let dealPhotoUrl = placeholderImage;
    if (this.props.deal.entityPhotos.length !== 0) dealPhotoUrl = this.props.deal.entityPhotos[0].getPhotoUrl();

    let brandPhotoUrl = placeholderBrandImage;
    if (this.props.deal.outlet.brand.entityPhotos.length !== 0) brandPhotoUrl = this.props.deal.outlet.brand.entityPhotos[0].getPhotoUrl();

    return (
      <div style={{
        marginBottom: this.props.bottomMargin,
        backgroundColor: ColorConstants.neutralOffWhite,
        position: 'relative',

        // width: DimensionConstants.dealCardImageWidth,
        height: DimensionConstants.dealCardImageHeight,
      }}>
        <Link style={{
          textDecoration: 'none',
        }}
              to={'deals/' + this.props.deal.id}>
          <div style={{
            display: 'flex',
          }}>

            <img
              src={dealPhotoUrl}
              alt={'Deal ID ' + this.props.deal.id}
              style={{
                width: DimensionConstants.dealCardImageWidth,
                minWidth: DimensionConstants.dealCardImageWidth,
                height: DimensionConstants.dealCardImageHeight,
                objectFit: 'cover',
                // display: 'block',
              }}
            />

            <div style={{
              display: 'flex',
              padding: '28px',
            }}>
              <BrandAvatar
                image={brandPhotoUrl}
                size={'29px'}
              />
              <div style={{
                fontFamily: 'Libre Franklin',
                width: '100%',
                height: '100%',
                marginLeft: '8px',
                marginTop: '4px',
              }}>

                <div style={{
                  fontSize: '16px',
                  fontWeight: '700',
                  color: '#36173b',
                }}>{this.props.deal.title}
                </div>

                {/*<div>{this.props.deal.getMainCategory() === undefined ? undefined : this.props.deal.getMainCategory().name}</div>*/}

                <div style={{
                  marginTop: '16px',
                  fontSize: '14px',
                  fontWeight: '300',
                  color: '#4a4a4a',
                }}>{this.props.deal.outlet.brand.name}
                </div>

                <div style={{
                  marginTop: '16px',
                }}>

                  <span style={{
                    fontSize: '14px',
                    fontWeight: '500',
                    textDecoration: 'line-through',
                    color: '#4a4a4a',
                    marginRight: '4px',
                  }}>{this.props.deal.getOriginalPrice()}</span>
                  <span style={{
                    fontSize: '16px',
                    fontWeight: '700',
                    color: '#417505',
                  }}>{this.props.deal.getDiscountedPrice()}</span>

                </div>
              </div>
            </div>

          </div>
        </Link>
      </div>
    );
  }
}

DealCardLandscapeWide.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
  bottomMargin: PropTypes.string.isRequired,
};
