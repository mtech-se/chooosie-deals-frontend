import { Grid } from '@material-ui/core/es/index';
import React, { Component } from 'react';
import { DealFactory } from '../factories/DealFactory';
import { DealService } from '../services/DealService';
import { BaseProgressLoader } from './BaseProgressLoader';
import { Carousel } from './Carousel/Carousel';
import { NoRecordsFound } from './NoRecordsFound';

export class RecommendedDeals extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasFetchedData: false,
      apiCallHasError: false,
      deals: undefined,
      currentPerPage: undefined,
      currentPageNumber: undefined,
      isAllDataFetched: false,
    };

    this.fetchMore = this.fetchMore.bind(this);
  }

  async componentDidMount() {
    const defaultPerPage = 10;
    const defaultPageNumber = 0;

    const response = await DealService.getRecommendedDeals(defaultPerPage, defaultPageNumber);

    const deals = DealFactory.createFromJsonArray(response.data.content);

    this.setState({
      hasFetchedData: true,
      deals,
      currentPerPage: defaultPerPage,
      currentPageNumber: defaultPageNumber,
    });
  }

  async fetchMore() {
    if (this.state.isAllDataFetched) return;

    const response = await DealService.getRecommendedDeals(this.state.currentPerPage, this.state.currentPageNumber + 1);

    const newlyFetchedDeals = DealFactory.createFromJsonArray(response.data.content);

    this.setState({
      deals: this.state.deals.concat(newlyFetchedDeals),
      currentPageNumber: newlyFetchedDeals.length !== 0 ? this.state.currentPageNumber + 1 : this.state.currentPageNumber,
      isAllDataFetched: newlyFetchedDeals.length === 0,
    });
  }

  render() {
    if (this.state.apiCallHasError === true) return <div style={{ textAlign: 'center' }}>error detected</div>;
    if (this.state.hasFetchedData === false) return <BaseProgressLoader/>;
    if (this.state.deals.length === 0) return <NoRecordsFound itemLabel={'recommended deals'}/>;

    return (
      <Grid container
            justify={'center'}>
        <Carousel
          label={'Recommended deals'}
          deals={this.state.deals}
          numItemsToShow={4}
          numItemsToScrollPerClick={4}
          itemWidth={224}
          marginBetweenItems={16}
          onFetchMore={this.fetchMore}
        />
      </Grid>
    );
  }
}