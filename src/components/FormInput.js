import * as PropTypes from 'prop-types';
import React from 'react';
import { Col, FormGroup, Input, Label } from 'reactstrap';

export const FormInput = props => (
  <FormGroup row>
    <Label xs="2">{props.label}</Label>
    <Col xs="10">
      <Input type={props.type}
             value={props.value}
             onChange={props.onChange}
      />
    </Col>
  </FormGroup>
);

FormInput.propTypes = {
  type: PropTypes.oneOf(['text', 'date', 'number', 'file']),
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};