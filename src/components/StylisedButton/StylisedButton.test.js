import { createShallow } from '@material-ui/core/test-utils';
// import { shallow } from 'enzyme';
import React from 'react';
import { StylisedButton } from './StylisedButton';

describe('StylisedButton', () => {
  let shallow;

  beforeEach(() => {
    shallow = createShallow();
  });

  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<StylisedButton colour={'red'}
                            title={'test'}/>);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});