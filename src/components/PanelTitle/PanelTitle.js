import PropTypes from 'prop-types';
import React from 'react';

export const PanelTitle = props => {
  return (
    <div style={{
      fontSize: '21px',
      fontFamily: 'Roboto, sans-serif',
      marginTop: props.marginTop !== undefined ? props.marginTop : '16px',
      marginBottom: props.marginBottom !== undefined ? props.marginBottom : '16px'
    }}>{props.title}</div>
  );
};

PanelTitle.propTypes = {
  title: PropTypes.string.isRequired,
  marginTop: PropTypes.string,
  marginBottom: PropTypes.string
};