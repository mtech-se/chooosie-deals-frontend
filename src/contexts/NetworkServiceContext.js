import React from 'react';

const defaultValue = {
  error: {
    message: 'network error message goes here',
  },
  setError: () => {},
};

export const NetworkServiceContext = React.createContext(defaultValue.dark);

export const withNetworkServiceContext = Component => function ComponentNeedingNetworkService(props) {
  return (
    <NetworkServiceContext.Consumer>
      {networkServiceContext => <Component {...props}
                                           networkServiceContext={networkServiceContext}/>}
    </NetworkServiceContext.Consumer>
  );
};