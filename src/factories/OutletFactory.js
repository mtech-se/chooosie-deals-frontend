import { Outlet } from '../models/Outlet';
import { BrandFactory } from './BrandFactory';
import { DateFactory } from './DateFactory';

export class OutletFactory {
  /**
   *
   * @param {string} json
   * @returns {Outlet}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Outlet(
      json.id,
      json.name,
      json.address,
      BrandFactory.createFromJson(json.brand),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Outlet[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => OutletFactory.createFromJson(json));
  }
}