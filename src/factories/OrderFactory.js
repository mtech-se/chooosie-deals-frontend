import { Order } from '../models/Order/Order';
import { CustomerFactory } from './CustomerFactory';
import { DateFactory } from './DateFactory';
import { OrderItemFactory } from './OrderItemFactory';
import { PaymentFactory } from './PaymentFactory';

export class OrderFactory {
  /**
   *
   * @param {string} json
   * @returns {*}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Order(
      json.id,
      CustomerFactory.createFromJson(json.customer),
      OrderItemFactory.createFromJsonArray(json.order_items),
      PaymentFactory.createFromJsonArray(json.payments),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param jsonArray
   * @returns {*}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => OrderFactory.createFromJson(json));
  }
}