import { Location } from '../models/Location';

export class LocationFactory {
  static createLocations(locationsJson) {
    return locationsJson.map(locationJson => {
      return new Location(locationJson);
    });
  }
}