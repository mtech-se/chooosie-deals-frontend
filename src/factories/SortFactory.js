import { Sort } from '../models/Sort';

export class SortFactory {
  static createFromJson(json) {
    if (json === undefined) return undefined;

    return new Sort(
      json.sorted,
      json.unsorted,
      json.empty
    );
  }
}