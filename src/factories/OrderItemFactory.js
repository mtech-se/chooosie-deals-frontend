import { OrderItem } from '../models/OrderItem';
import { DateFactory } from './DateFactory';
import { DealFactory } from './DealFactory';

export class OrderItemFactory {
  /**
   *
   * @param {string} json
   * @returns {OrderItem}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new OrderItem(
      json.id,
      DealFactory.createFromJson(json.deal),
      json.spot_price,
      DateFactory.createMomentFromDateString(json.redeemed_at),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {OrderItem[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => OrderItemFactory.createFromJson(json));
  }
}