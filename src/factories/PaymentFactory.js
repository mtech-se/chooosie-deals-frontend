import { Payment } from '../models/Payment';
import { DateFactory } from './DateFactory';
import { OrderFactory } from './OrderFactory';

export class PaymentFactory {
  /**
   *
   * @param {string} json
   * @returns {Payment}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Payment(
      json.id,
      OrderFactory.createFromJson(json.order),
      json.receipt_amount,
      json.spot_billing_address,
      DateFactory.createMomentFromDateString(json.completed_at),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Payment[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => PaymentFactory.createFromJson(json));
  }
}