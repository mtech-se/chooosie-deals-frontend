import { Merchant } from '../models/Merchant';
import { ContactPersonFactory } from './ContactPersonFactory';
import { DateFactory } from './DateFactory';

export class MerchantFactory {
  /**
   *
   * @param {string} json
   * @returns {Merchant}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Merchant(
      json.id,
      json.name,
      json.business_registration_number,
      json.mailing_address,
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
      ContactPersonFactory.createFromJsonArray(json.contact_persons)
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Merchant[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => MerchantFactory.createFromJson(json));
  }
}

