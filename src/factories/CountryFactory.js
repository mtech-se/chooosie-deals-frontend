import { Country } from '../models/Country';

export class CountryFactory {
  static createCountries(countriesJson) {
    return countriesJson.map(countryJson => {
      return new Country(countryJson);
    });
  }
}