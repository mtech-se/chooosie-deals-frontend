import { SpringPagination } from '../models/SpringPagination';
import { PageableFactory } from './PageableFactory';
import { SortFactory } from './SortFactory';

export class SpringPaginationFactory {
  /**
   *
   * @param json
   * @returns {SpringPagination}
   */
  static createFromJson(json) {
    if (json === undefined) return undefined;

    return new SpringPagination(
      PageableFactory.createFromJson(json.pageable),
      json.totalPages,
      json.last,
      json.totalElements,
      json.size,
      json.number,
      SortFactory.createFromJson(json.sort),
      json.numberOfElements,
      json.first,
      json.empty
    );
  }
}