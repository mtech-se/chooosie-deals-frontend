export class OrderItemNotFound extends Error {
  constructor(message) {
    super();

    this.message = message;
  }
}