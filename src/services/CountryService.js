import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class CountryService {
  static getCountries() {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/countries')
      .then(response => {
        return response;
      });
  }
}