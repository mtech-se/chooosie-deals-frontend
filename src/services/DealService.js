import * as queryString from 'query-string';
import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class DealService {
  /**
   *
   * @param {number} size
   * @param {number} page
   * @param {string} title
   * @returns {Promise<*>}
   */
  static getDeals(size = undefined, page = undefined, title = undefined) {
    const url = `${ApiConstants.BASE_URL}/deals`;

    const axiosOptions = {
      params: {
        size,
        page,
        title,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  static getDealsOfTheMonth() {
    const url = `${ApiConstants.BASE_URL}/deals`;

    const axiosOptions = {
      params: {
        dotm: true,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param {number} id
   * @returns {Promise<AxiosResponse<any>>}
   */
  static getRecommendedDeals(id) {
    const url = `${ApiConstants.BASE_URL}/deals/${id}/recommended`;

    return AxiosSingleton.getAuthAxiosInstance().get(url);
  }

  static getDeal(dealId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/deals/' + dealId);
  }

  /**
   *
   * @param {Object} queryParams
   * @returns {Promise<AxiosResponse<any>>}
   */
  static searchDeals(queryParams) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/deals?' + queryString.stringify(queryParams));
  }

  static incrementHitCount(dealId, userId) {
    const data = {
      deal_id: dealId,
      user_id: userId,
    };

    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/deals/dealclicked', data);
  }
}