import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class LoginService {
  /**
   *
   * @returns {AxiosPromise<any>}
   * @param {string} email
   * @param {string} password
   */
  static login(email, password) {
    const data = {
      email: email,
      password: password,
      type: process.env.REACT_APP_LOGIN_TYPE
    };

    return AxiosSingleton.getGuestAxiosInstance().post(ApiConstants.BASE_URL + '/login', data);
  }
}
