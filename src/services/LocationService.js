import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class LocationService {
  static getLocations() {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/locations')
      .then(response => {
        return response;
      });
  }
}