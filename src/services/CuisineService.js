import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class CuisineService {
  static getCuisines() {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/cuisines')
      .then(response => {
        return response;
      });
  }
}