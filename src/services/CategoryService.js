import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class CategoryService {
  static getCategories() {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/categories')
      .then(response => {
        return response;
      });
  }
}