import AxiosSingleton from '../AxiosSingleton';
import ApiConstants from '../constants/ApiConstants';

export default class PurchaseService {
  static getPurchases() {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/purchases')
      .then(response => {
        return response;
      });
  }
}