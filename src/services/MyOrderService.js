import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class MyOrderService {

  /**
   *
   * @returns {AxiosPromise<any>}
   */
  static getCurrentOrder() {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/my-orders?current=true');
  }

  /**
   *
   * @returns {AxiosPromise<any>}
   */
  static getAllOrders() {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/my-orders');
  }

  /**
   *
   * @param orderId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static makePayment(orderId, data) {
    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/my-orders/' + orderId + '/payments', data);
  }

  /**
   *
   * @returns {AxiosPromise<any>}
   */
  static createNewOrder() {
    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/my-orders/', undefined);
  }

  /**
   *
   * @param orderId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static updateOrder(orderId, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(ApiConstants.BASE_URL + '/my-orders/' + orderId, data);
  }

  /**
   *
   * @param orderItemId
   * @returns {AxiosPromise<any>}
   */
  static redeemOrder(orderItemId) {
    return AxiosSingleton.getAuthAxiosInstance().put(ApiConstants.BASE_URL + '/my-orders/redeem-item/' + orderItemId);
  }
}