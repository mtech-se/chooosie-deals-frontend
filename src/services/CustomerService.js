import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class CustomerService {

  /**
   *
   * @param {object} data
   * @returns {AxiosPromise<any>}
   */
  static createCustomer(data) {
    return AxiosSingleton.getGuestAxiosInstance().post(ApiConstants.BASE_URL + '/customers', data);
  }

  /**
   *
   * @param {number} id
   * @param {object} data
   * @returns {AxiosPromise<any>}
   */
  static updateCustomer(id, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(`${ApiConstants.BASE_URL}/customers/${id}`, data);
  }
}