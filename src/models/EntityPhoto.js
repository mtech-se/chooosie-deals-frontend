import { ApiConstants } from '../constants/ApiConstants';

export class EntityPhoto {
  /**
   *
   * @param {number} id
   * @param {Photo} photo
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    photo,
    createdAt,
    updatedAt,
    deletedAt
  ) {
    this.id = id;
    this.photo = photo;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }

  /** @returns {string} */
  getPhotoUrl() {
    if (this.photo.deletedAt !== null) return undefined;

    return `${ApiConstants.BASE_URL}/photos/${this.photo.id}`;
  }
}