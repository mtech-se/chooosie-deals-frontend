import { CurrencyUtils } from '../utils/CurrencyUtils';
import { StringUtils } from '../utils/StringUtils';

export class Deal {
  /**
   *
   * @param {number} id
   * @param {string} title
   * @param {string} description
   * @param {number} originalPrice
   * @param {number} discountedPrice
   * @param {Outlet} outlet
   * @param {number} maxQuantity
   * @param {number} numBought
   * @param {string} redemptionInstructions
   * @param {string} termsOfUse
   * @param {EntityPhoto[]} entityPhotos
   * @param {boolean} dealOfTheMonth
   * @param {number} hitCount
   * @param {CategoryDeal[]} categoryDeals
   * @param {CuisineDeal[]} cuisineDeals
   * @param {moment} autoPublishDate
   * @param {moment} redemptionExpiryDate
   * @param {moment} createdAt
   * @param {moment} publishedAt
   * @param {moment} archivedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    title,
    description,
    originalPrice,
    discountedPrice,
    outlet,
    maxQuantity,
    numBought,
    redemptionInstructions,
    termsOfUse,
    entityPhotos,
    dealOfTheMonth,
    hitCount,
    categoryDeals,
    cuisineDeals,
    autoPublishDate,
    redemptionExpiryDate,
    createdAt,
    publishedAt,
    archivedAt,
    deletedAt,
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.originalPrice = originalPrice;
    this.discountedPrice = discountedPrice;
    this.outlet = outlet;
    this.maxQuantity = maxQuantity;
    this.numBought = numBought;
    this.redemptionInstructions = redemptionInstructions;
    this.termsOfUse = termsOfUse;
    this.entityPhotos = entityPhotos;
    this.dealOfTheMonth = dealOfTheMonth;
    this.hitCount = hitCount;
    this.categoryDeals = categoryDeals;
    this.cuisineDeals = cuisineDeals;
    this.autoPublishDate = autoPublishDate;
    this.redemptionExpiryDate = redemptionExpiryDate;
    this.createdAt = createdAt;
    this.publishedAt = publishedAt;
    this.archivedAt = archivedAt;
    this.deletedAt = deletedAt;
  }

  getOriginalPrice() {
    return CurrencyUtils.toCurrency(this.originalPrice / 100);
  }

  getDiscountedPrice() {
    return CurrencyUtils.toCurrency(this.discountedPrice / 100);
  }

  getDiscountPercentage() {
    return ((this.originalPrice - this.discountedPrice) / this.originalPrice).toLocaleString('en-sg', { style: 'percent' });
  }

  getTitleSlug() {
    return StringUtils.slugify(this.title);
  }

  /** @returns {string} */
  getMainPhotoUrl() {
    if (this.entityPhotos === undefined) return undefined;
    if (this.entityPhotos.length === 0) return undefined;

    return this.entityPhotos[0].getPhotoUrl();
  }

  /** @returns {Category} */
  getMainCategory() {
    if (this.categoryDeals === undefined) return undefined;
    if (this.categoryDeals.length === 0) return undefined;

    return this.categoryDeals[0].category;
  }

  toJson() {
    return {
      'title': this.title,
      'description': this.description,
      'original_price': this.originalPrice,
      'discounted_price': this.discountedPrice,
      'outlet': this.outlet === undefined ? undefined : {
        'id': this.outlet.id
      },
      'category_deals': this.categoryDeals,
      'cuisine_deals': this.cuisineDeals,
      'max_quantity': this.maxQuantity,
      'redemption_instructions': this.redemptionInstructions,
      'terms_of_use': this.termsOfUse,
      'auto_publish_date': this.autoPublishDate,
      'redemption_expiry_date': this.redemptionExpiryDate,
    };
  }
}