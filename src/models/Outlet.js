export class Outlet {
  /**
   *
   * @param {number} id
   * @param {string} name
   * @param {string} address
   * @param {Brand} brand
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    name,
    address,
    brand,
    createdAt,
    updatedAt,
    deletedAt
  ) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.brand = brand;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}