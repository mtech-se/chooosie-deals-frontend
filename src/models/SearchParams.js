import * as queryString from 'query-string';

export class SearchParams {
  constructor() {
    this.searchTerm = undefined;
    this.countryId = undefined;
    this.locationId = undefined;
    this.cuisineId = undefined;
    this.priceMin = undefined;
    this.priceMax = undefined;
    this.categoryId = undefined;
  }

  toUrlComponent() {
    return queryString.stringify(this);
  }
}