export class SpringPagination {
  /**
   *
   * @param {Pageable} pageable
   * @param totalPages
   * @param last
   * @param totalElements
   * @param size
   * @param number
   * @param sort
   * @param numberOfElements
   * @param first
   * @param empty
   */
  constructor(
    pageable,
    totalPages,
    last,
    totalElements,
    size,
    number,
    sort,
    numberOfElements,
    first,
    empty
  ) {
    this.pageable = pageable;
    this.totalPages = totalPages;
    this.last = last;
    this.totalElements = totalElements;
    this.size = size;
    this.number = number;
    this.sort = sort;
    this.numberOfElements = numberOfElements;
    this.first = first;
    this.empty = empty;
  }
}