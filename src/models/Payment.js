export class Payment {
  /**
   *
   * @param {number} id
   * @param {Order} order
   * @param {number} receiptAmount
   * @param {string} spotBillingAddress
   * @param {moment} completedAt
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    order,
    receiptAmount,
    spotBillingAddress,
    completedAt,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.order = order;
    this.receiptAmount = receiptAmount;
    this.spotBillingAddress = spotBillingAddress;
    this.completedAt = completedAt;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}