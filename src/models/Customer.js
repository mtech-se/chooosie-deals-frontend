export class Customer {
  /**
   *
   * @param {number} id
   * @param {string} firstName
   * @param {string} lastName
   * @param {string} email
   * @param {string} gender
   * @param {string} billingFirstName
   * @param {string} billingLastName
   * @param {string} billingCellphone
   * @param {string} billingTelephone
   * @param {string} billingAddressLineOne
   * @param {string} billingAddressLineTwo
   * @param {string} billingAddressPostalCode
   * @param {string} createdAt
   * @param {string} updatedAt
   * @param {string} deletedAt
   */
  constructor(
    id = undefined,
    firstName = undefined,
    lastName = undefined,
    email = undefined,
    gender = undefined,
    billingFirstName = undefined,
    billingLastName = undefined,
    billingCellphone = undefined,
    billingTelephone = undefined,
    billingAddressLineOne = undefined,
    billingAddressLineTwo = undefined,
    billingAddressPostalCode = undefined,
    createdAt = undefined,
    updatedAt = undefined,
    deletedAt = undefined,
  ) {
    this.id = id;
    this.name = firstName + ' ' + lastName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.gender = gender;
    this.billingFirstName = billingFirstName;
    this.billingLastName = billingLastName;
    this.billingCellphone = billingCellphone;
    this.billingTelephone = billingTelephone;
    this.billingAddressLineOne = billingAddressLineOne;
    this.billingAddressLineTwo = billingAddressLineTwo;
    this.billingAddressPostalCode = billingAddressPostalCode;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}