export class Sort {
  /**
   *
   * @param {boolean} sorted
   * @param {boolean} unsorted
   * @param {boolean} empty
   */
  constructor(sorted, unsorted, empty) {
    this.sorted = sorted;
    this.unsorted = unsorted;
    this.empty = empty;
  }
}