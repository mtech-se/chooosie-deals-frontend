import { OrderItemNotFound } from '../../errors/OrderItemNotFound';

export class Order {
  /**
   *
   * @param {number} id
   * @param {Customer} customer
   * @param {OrderItem[]} orderItems
   * @param {Payment[]} payments
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    customer,
    orderItems,
    payments,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.customer = customer;
    this.orderItems = orderItems;
    this.payments = payments;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }

  // /**
  //  *
  //  * @param {OrderItem} orderItem
  //  */
  // addOrderItem(orderItem) {
  //   this.orderItems.push(orderItem);
  // }

  /**
   *
   * @param {OrderItem} orderItem
   */
  removeOrderItem(orderItem) {
    if (orderItem.id !== undefined) {
      this.orderItems = this.orderItems.filter(currentOrderItem => currentOrderItem.id !== orderItem.id);
      return;
    }

    const dealIdOfOrderItemToBeRemoved = orderItem.deal.id;

    let indexToRemove = undefined;

    for (let i = this.orderItems.length - 1; i >= 0; i--) {
      const dealIdOfThisOrderItem = this.orderItems[i].deal.id;

      if (dealIdOfThisOrderItem === dealIdOfOrderItemToBeRemoved) {
        indexToRemove = i;
        break;
      }
    }

    if (indexToRemove === undefined) throw new OrderItemNotFound(`Cannot find orderItem for deal: ${dealIdOfOrderItemToBeRemoved.id}`);

    this.orderItems.splice(indexToRemove, 1);
  }

  // /**
  //  * Used only in the add-to-cart button on the single-deal view.
  //  *
  //  * @param {OrderItem[]} orderItems
  //  */
  // addOrderItems(orderItems) {
  //   this.orderItems = this.orderItems.concat(orderItems);
  // }

  // /**
  //  *
  //  * @param {Deal} deal
  //  */
  // removeAllOrderItemsForDeal(deal) {
  //   this.orderItems = this.orderItems.filter(currentItem => {
  //     const currentItemDealId = currentItem.deal.id;
  //     return currentItemDealId !== deal.id;
  //   });
  // }
}