export const orderJson = '{\n' +
  '            "id": 49,\n' +
  '            "customer": {\n' +
  '                "id": 22,\n' +
  '                "created_at": "2019-01-26T04:16:58.000Z",\n' +
  '                "updated_at": "2019-01-26T04:16:58.000Z",\n' +
  '                "deleted_at": null,\n' +
  '                "email": "customer@customer.com",\n' +
  '                "first_name": "customer",\n' +
  '                "last_name": "customer",\n' +
  '                "gender": "male",\n' +
  '                "billing_first_name": "test",\n' +
  '                "billing_last_name": "test_last",\n' +
  '                "billing_cellphone": "test+cell",\n' +
  '                "billing_telephone": "test_tele",\n' +
  '                "billing_add_line_one": "flat test, floor test",\n' +
  '                "billing_add_line_two": "street test, city test",\n' +
  '                "billing_add_postal_code": "test_post"\n' +
  '            },\n' +
  '            "created_at": "2019-01-26T04:17:31.000Z",\n' +
  '            "updated_at": "2019-01-26T04:19:56.000Z",\n' +
  '            "deleted_at": null,\n' +
  '            "order_items": [\n' +
  '                {\n' +
  '                    "id": 292,\n' +
  '                    "deal": {\n' +
  '                        "id": 46,\n' +
  '                        "title": "(Weekend) BBQ Buffet for 1 Person",\n' +
  '                        "description": "Brevity is the soul of wit.",\n' +
  '                        "outlet": {\n' +
  '                            "id": 56,\n' +
  '                            "name": "Bojangles\' Famous Chicken \'n Biscuits",\n' +
  '                            "address": "726 Spencer Mission, Wymanshire",\n' +
  '                            "brand": {\n' +
  '                                "id": 79,\n' +
  '                                "name": "Bojangles\' Famous Chicken \'n Biscuits",\n' +
  '                                "merchant": {\n' +
  '                                    "id": 3,\n' +
  '                                    "name": "Bashirian-Bashirian",\n' +
  '                                    "created_at": "2019-01-26T04:15:24.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:24.000Z",\n' +
  '                                    "deleted_at": null,\n' +
  '                                    "business_registration_number": "ons79909179n",\n' +
  '                                    "mailing_address": "092 Borer Corners, New Desiree, FL 48928"\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:30.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:30.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            "created_at": "2019-01-26T04:15:30.000Z",\n' +
  '                            "updated_at": "2019-01-26T04:15:30.000Z",\n' +
  '                            "deleted_at": null\n' +
  '                        },\n' +
  '                        "created_at": "2018-08-18T08:36:20.000Z",\n' +
  '                        "updated_at": "2019-01-26T04:15:37.000Z",\n' +
  '                        "deleted_at": null,\n' +
  '                        "original_price": 1243,\n' +
  '                        "discounted_price": 981,\n' +
  '                        "terms_of_use": "I solemnly swear I am up to no good.",\n' +
  '                        "redemption_instructions": "Wisely and slow; they stumble that run fast.",\n' +
  '                        "max_quantity": 68,\n' +
  '                        "deal_of_the_month": false,\n' +
  '                        "category_deals": [],\n' +
  '                        "cuisine_deals": [],\n' +
  '                        "deal_photos": [\n' +
  '                            {\n' +
  '                                "id": 148,\n' +
  '                                "photo": {\n' +
  '                                    "id": 253,\n' +
  '                                    "path": "/deals/46/01.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 149,\n' +
  '                                "photo": {\n' +
  '                                    "id": 254,\n' +
  '                                    "path": "/deals/46/11.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 150,\n' +
  '                                "photo": {\n' +
  '                                    "id": 255,\n' +
  '                                    "path": "/deals/46/21.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 151,\n' +
  '                                "photo": {\n' +
  '                                    "id": 256,\n' +
  '                                    "path": "/deals/46/31.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 152,\n' +
  '                                "photo": {\n' +
  '                                    "id": 257,\n' +
  '                                    "path": "/deals/46/41.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            }\n' +
  '                        ],\n' +
  '                        "auto_publish_date": null,\n' +
  '                        "published_at": null,\n' +
  '                        "redemption_expiry_date": null,\n' +
  '                        "archived_at": null,\n' +
  '                        "hit_count": null,\n' +
  '                        "num_bought": null\n' +
  '                    },\n' +
  '                    "spotPrice": 981,\n' +
  '                    "created_at": "2019-01-26T04:19:56.000Z",\n' +
  '                    "updated_at": "2019-01-26T04:19:56.000Z",\n' +
  '                    "deleted_at": null,\n' +
  '                    "redeemed_at": null\n' +
  '                },\n' +
  '                {\n' +
  '                    "id": 293,\n' +
  '                    "deal": {\n' +
  '                        "id": 54,\n' +
  '                        "title": "20% Off",\n' +
  '                        "description": "A little more than kin, and less than kind.",\n' +
  '                        "outlet": {\n' +
  '                            "id": 140,\n' +
  '                            "name": "Sbarro 76",\n' +
  '                            "address": "7329 Lilly Grove, Port Sydnee",\n' +
  '                            "brand": {\n' +
  '                                "id": 70,\n' +
  '                                "name": "Sbarro",\n' +
  '                                "merchant": {\n' +
  '                                    "id": 8,\n' +
  '                                    "name": "Walker Inc",\n' +
  '                                    "created_at": "2019-01-26T04:15:25.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:25.000Z",\n' +
  '                                    "deleted_at": null,\n' +
  '                                    "business_registration_number": "jio55084316y",\n' +
  '                                    "mailing_address": "720 Harber Gateway, West Deion, CT 70103-8003"\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:30.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:30.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            "created_at": "2019-01-26T04:15:31.000Z",\n' +
  '                            "updated_at": "2019-01-26T04:15:31.000Z",\n' +
  '                            "deleted_at": null\n' +
  '                        },\n' +
  '                        "created_at": "2018-09-27T19:15:56.000Z",\n' +
  '                        "updated_at": "2019-01-26T04:15:37.000Z",\n' +
  '                        "deleted_at": null,\n' +
  '                        "original_price": 6400,\n' +
  '                        "discounted_price": 4672,\n' +
  '                        "terms_of_use": "Happiness can be found even in the darkest of times if only one remembers to turn on the light.",\n' +
  '                        "redemption_instructions": "Not stepping o\'er the bounds of modesty.",\n' +
  '                        "max_quantity": 70,\n' +
  '                        "deal_of_the_month": false,\n' +
  '                        "category_deals": [],\n' +
  '                        "cuisine_deals": [],\n' +
  '                        "deal_photos": [\n' +
  '                            {\n' +
  '                                "id": 175,\n' +
  '                                "photo": {\n' +
  '                                    "id": 280,\n' +
  '                                    "path": "/deals/54/01.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 176,\n' +
  '                                "photo": {\n' +
  '                                    "id": 281,\n' +
  '                                    "path": "/deals/54/11.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 177,\n' +
  '                                "photo": {\n' +
  '                                    "id": 282,\n' +
  '                                    "path": "/deals/54/21.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            }\n' +
  '                        ],\n' +
  '                        "auto_publish_date": null,\n' +
  '                        "published_at": null,\n' +
  '                        "redemption_expiry_date": null,\n' +
  '                        "archived_at": null,\n' +
  '                        "hit_count": null,\n' +
  '                        "num_bought": null\n' +
  '                    },\n' +
  '                    "spotPrice": 4672,\n' +
  '                    "created_at": "2019-01-26T04:19:56.000Z",\n' +
  '                    "updated_at": "2019-01-26T04:19:56.000Z",\n' +
  '                    "deleted_at": null,\n' +
  '                    "redeemed_at": null\n' +
  '                }\n' +
  '            ],\n' +
  '            "payments": []\n' +
  '        }';

export const orderItemJson = '{\n' +
  '                    "id": null,\n' +
  '                    "deal": {\n' +
  '                        "id": 46,\n' +
  '                        "title": "(Weekend) BBQ Buffet for 1 Person",\n' +
  '                        "description": "Brevity is the soul of wit.",\n' +
  '                        "outlet": {\n' +
  '                            "id": 56,\n' +
  '                            "name": "Bojangles\' Famous Chicken \'n Biscuits",\n' +
  '                            "address": "726 Spencer Mission, Wymanshire",\n' +
  '                            "brand": {\n' +
  '                                "id": 79,\n' +
  '                                "name": "Bojangles\' Famous Chicken \'n Biscuits",\n' +
  '                                "merchant": {\n' +
  '                                    "id": 3,\n' +
  '                                    "name": "Bashirian-Bashirian",\n' +
  '                                    "created_at": "2019-01-26T04:15:24.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:24.000Z",\n' +
  '                                    "deleted_at": null,\n' +
  '                                    "business_registration_number": "ons79909179n",\n' +
  '                                    "mailing_address": "092 Borer Corners, New Desiree, FL 48928"\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:30.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:30.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            "created_at": "2019-01-26T04:15:30.000Z",\n' +
  '                            "updated_at": "2019-01-26T04:15:30.000Z",\n' +
  '                            "deleted_at": null\n' +
  '                        },\n' +
  '                        "created_at": "2018-08-18T08:36:20.000Z",\n' +
  '                        "updated_at": "2019-01-26T04:15:37.000Z",\n' +
  '                        "deleted_at": null,\n' +
  '                        "original_price": 1243,\n' +
  '                        "discounted_price": 981,\n' +
  '                        "terms_of_use": "I solemnly swear I am up to no good.",\n' +
  '                        "redemption_instructions": "Wisely and slow; they stumble that run fast.",\n' +
  '                        "max_quantity": 68,\n' +
  '                        "deal_of_the_month": false,\n' +
  '                        "category_deals": [],\n' +
  '                        "cuisine_deals": [],\n' +
  '                        "deal_photos": [\n' +
  '                            {\n' +
  '                                "id": 148,\n' +
  '                                "photo": {\n' +
  '                                    "id": 253,\n' +
  '                                    "path": "/deals/46/01.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 149,\n' +
  '                                "photo": {\n' +
  '                                    "id": 254,\n' +
  '                                    "path": "/deals/46/11.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 150,\n' +
  '                                "photo": {\n' +
  '                                    "id": 255,\n' +
  '                                    "path": "/deals/46/21.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 151,\n' +
  '                                "photo": {\n' +
  '                                    "id": 256,\n' +
  '                                    "path": "/deals/46/31.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 152,\n' +
  '                                "photo": {\n' +
  '                                    "id": 257,\n' +
  '                                    "path": "/deals/46/41.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            }\n' +
  '                        ],\n' +
  '                        "auto_publish_date": null,\n' +
  '                        "published_at": null,\n' +
  '                        "redemption_expiry_date": null,\n' +
  '                        "archived_at": null,\n' +
  '                        "hit_count": null,\n' +
  '                        "num_bought": null\n' +
  '                    },\n' +
  '                    "spotPrice": 981,\n' +
  '                    "created_at": "2019-01-26T04:19:56.000Z",\n' +
  '                    "updated_at": "2019-01-26T04:19:56.000Z",\n' +
  '                    "deleted_at": null,\n' +
  '                    "redeemed_at": null\n' +
  '                }';

export const dealJson = '{\n' +
  '                        "id": 54,\n' +
  '                        "title": "20% Off",\n' +
  '                        "description": "A little more than kin, and less than kind.",\n' +
  '                        "outlet": {\n' +
  '                            "id": 140,\n' +
  '                            "name": "Sbarro 76",\n' +
  '                            "address": "7329 Lilly Grove, Port Sydnee",\n' +
  '                            "brand": {\n' +
  '                                "id": 70,\n' +
  '                                "name": "Sbarro",\n' +
  '                                "merchant": {\n' +
  '                                    "id": 8,\n' +
  '                                    "name": "Walker Inc",\n' +
  '                                    "created_at": "2019-01-26T04:15:25.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:25.000Z",\n' +
  '                                    "deleted_at": null,\n' +
  '                                    "business_registration_number": "jio55084316y",\n' +
  '                                    "mailing_address": "720 Harber Gateway, West Deion, CT 70103-8003"\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:30.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:30.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            "created_at": "2019-01-26T04:15:31.000Z",\n' +
  '                            "updated_at": "2019-01-26T04:15:31.000Z",\n' +
  '                            "deleted_at": null\n' +
  '                        },\n' +
  '                        "created_at": "2018-09-27T19:15:56.000Z",\n' +
  '                        "updated_at": "2019-01-26T04:15:37.000Z",\n' +
  '                        "deleted_at": null,\n' +
  '                        "original_price": 6400,\n' +
  '                        "discounted_price": 4672,\n' +
  '                        "terms_of_use": "Happiness can be found even in the darkest of times if only one remembers to turn on the light.",\n' +
  '                        "redemption_instructions": "Not stepping o\'er the bounds of modesty.",\n' +
  '                        "max_quantity": 70,\n' +
  '                        "deal_of_the_month": false,\n' +
  '                        "category_deals": [],\n' +
  '                        "cuisine_deals": [],\n' +
  '                        "deal_photos": [\n' +
  '                            {\n' +
  '                                "id": 175,\n' +
  '                                "photo": {\n' +
  '                                    "id": 280,\n' +
  '                                    "path": "/deals/54/01.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 176,\n' +
  '                                "photo": {\n' +
  '                                    "id": 281,\n' +
  '                                    "path": "/deals/54/11.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            },\n' +
  '                            {\n' +
  '                                "id": 177,\n' +
  '                                "photo": {\n' +
  '                                    "id": 282,\n' +
  '                                    "path": "/deals/54/21.png",\n' +
  '                                    "created_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "updated_at": "2019-01-26T04:15:48.000Z",\n' +
  '                                    "deleted_at": null\n' +
  '                                },\n' +
  '                                "created_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "updated_at": "2019-01-26T04:15:49.000Z",\n' +
  '                                "deleted_at": null\n' +
  '                            }\n' +
  '                        ],\n' +
  '                        "auto_publish_date": null,\n' +
  '                        "published_at": null,\n' +
  '                        "redemption_expiry_date": null,\n' +
  '                        "archived_at": null,\n' +
  '                        "hit_count": null,\n' +
  '                        "num_bought": null\n' +
  '                    }';