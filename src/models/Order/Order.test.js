import { DealFactory } from '../../factories/DealFactory';
import { OrderFactory } from '../../factories/OrderFactory';
import { OrderItemFactory } from '../../factories/OrderItemFactory';
import { dealJson, orderItemJson, orderJson } from './jsons';
import { Order } from './Order';

describe('Order', () => {
  /** @var {Order} order */
  let order;

  /** @var {OrderItem} orderItem */
  let orderItem;

  /** @var {OrderItem} orderItem2 */
  let orderItem2;

  /** @var {Deal} deal */
  let deal;

  beforeEach(() => {
    order = OrderFactory.createFromJson(JSON.parse(orderJson));
    orderItem = OrderItemFactory.createFromJson(JSON.parse(orderItemJson));
    orderItem2 = OrderItemFactory.createFromJson(JSON.parse(orderItemJson));
    deal = DealFactory.createFromJson(JSON.parse(dealJson));

    expect(order.id).toBe(49);
    expect(order.orderItems.length).toBe(2);
    expect(order.orderItems[0].id).toBe(292);
    expect(order.orderItems[1].id).toBe(293);

    expect(orderItem.id).toBeNull();
  });

  // it('can add order item', () => {
  //   // given an order with 2 order_items 292 and 293
  //   // and an orderItem (that has no ID)
  //   const numItemsBefore = order.orderItems.length;
  //
  //   // when we add the orderItem to the order
  //   order.addOrderItem(orderItem);
  //
  //   // then we expect the order's orderItems to have 1 more orderItem
  //   // and that last item is the orderItem we added
  //   expect(order.orderItems.length).toBe(numItemsBefore + 1);
  //   expect(order.orderItems[order.orderItems.length - 1]).toBe(orderItem);
  // });

  it('can remove order item', () => {
    // given an order with 2 order_items 292 and 293
    // and an orderItem (that has no ID)
    const numItemsBefore = order.orderItems.length;

    // when we remove the orderItem to the order
    order.removeOrderItem(orderItem);

    // then we expect the order's orderItems to have 1 less orderItem
    expect(order.orderItems.length).toBe(numItemsBefore - 1);
  });

  // it('can add an array of order items', () => {
  //   // given an order with 2 order_items 292 and 293
  //   // and 2 orderItem (that has no ID)
  //   const numItemsBefore = order.orderItems.length;
  //
  //   // when we add the array to the order
  //   order.addOrderItems([orderItem, orderItem2]);
  //
  //   // then we expect the order's orderItems to have 2 more orderItems
  //   expect(order.orderItems.length).toBe(numItemsBefore + 2);
  // });

  // it('can remove all order items belonging to a deal', () => {
  //   // given an order with 2 order_items 292 (deal ID 46) and 293 (deal ID 54)
  //
  //   // when we remove all order items for that deal from the order
  //   order.removeAllOrderItemsForDeal(deal);
  //
  //   // then we expect the order's orderItems to have 1 orderItems
  //   expect(order.orderItems.length).toBe(1);
  // });

  // it('can get deals with order items', () => {
  //   // given an order with 2 order_items 292 (deal ID 46) and 293 (deal ID 54)
  //
  //   // when we get an array of deals with order_items associated
  //   const dealsWithOrderItems = order.getDealsWithOrderItems();
  //
  //   // then we expect to get an array of 2 deal elements
  //   // with each having 1 orderItems
  //   expect(dealsWithOrderItems.length).toBe(2);
  //   expect(dealsWithOrderItems[0].orderItems.length).toBe(1);
  //   expect(dealsWithOrderItems[1].orderItems.length).toBe(1);
  // });
});