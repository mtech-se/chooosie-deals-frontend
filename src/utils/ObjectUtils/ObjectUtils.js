/**
 *
 * @param {object} source
 * @param {object} destination
 * @returns {void}
 */
const mapValues = (source, destination) => {
  for (let key in destination) {
    if (source[key] !== undefined) destination[key] = source[key];
  }
};

export default {
  mapValues,
};
