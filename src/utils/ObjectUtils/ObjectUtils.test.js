import { Customer } from '../../models/Customer';
import ObjectUtils from './ObjectUtils';

describe('ObjectUtils.vue', () => {
  it('can map values of an object literal to an class instance', () => {
    const source = {
      firstName: 'xxx',
      lastName: 'xxx',
    };

    const destination = new Customer();

    expect(destination.firstName).toBe(undefined);
    expect(destination.lastName).toBe(undefined);

    ObjectUtils.mapValues(source, destination);

    expect(destination.firstName).toBe(source.firstName);
    expect(destination.lastName).toBe(source.lastName);
  });

  it('can read and write values of a class instance using the array index syntax', () => {
    const customer = new Customer();
    customer.firstName = 'xxx';
    customer.lastName = 'yyy';

    console.log(customer);

    expect(customer.firstName).toBe('xxx');
    expect(customer['firstName']).toBe('xxx');
  });
});