// /**
//  * Returns the newer of 2 date strings
//  * @param dateString1 String Can be any format that moment() accepts
//  * @param dateString2 String Can be any format that moment() accepts
//  * @returns {string}
//  */
// export function getNewerDateString(dateString1, dateString2) {
//   const incomingDate = moment(dateString1);
//   const reigningDate = moment(dateString2);
//
//   return incomingDate.isSameOrAfter(reigningDate) ? incomingDate.format(DateConstants.MYSQL_DATE_FORMAT) : reigningDate.format(DateConstants.MYSQL_DATE_FORMAT);
// }

/**
 *
 * @param momentDate
 * @returns {*}
 */
export function toFriendlyDateString(momentDate) {
  if (momentDate === undefined) return undefined;
  return momentDate.format('DD MMM YYYY');
}