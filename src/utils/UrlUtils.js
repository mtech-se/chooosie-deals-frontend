import * as queryString from 'query-string';
import { PaginationRequestParams } from '../models/PaginationRequestParams';
import { SearchParams } from '../models/SearchParams';

/**
 *
 * @param {string} theQueryString
 * @returns {SearchParams}
 */
export const getSearchParamsFromQueryString = theQueryString => {
  const queryStrings = queryString.parse(theQueryString);
  const searchParams = new SearchParams();
  searchParams.searchTerm = queryStrings.searchTerm;
  searchParams.countryId = queryStrings.countryId;
  searchParams.locationId = queryStrings.locationId;
  searchParams.cuisineId = queryStrings.cuisineId;
  searchParams.priceMin = queryStrings.priceMin;
  searchParams.priceMax = queryStrings.priceMax;
  searchParams.categoryId = queryStrings.categoryId;
  return searchParams;
};

/**
 *
 * @param {string} theQueryString
 * @returns {PaginationRequestParams}
 */
export const getPaginationParamsFromQueryString = theQueryString => {
  const queryStrings = queryString.parse(theQueryString);

  const size = queryStrings.size === undefined ? 10 : queryStrings.size;
  const page = queryStrings.page === undefined ? 0 : queryStrings.page;

  return new PaginationRequestParams(size, page);
};