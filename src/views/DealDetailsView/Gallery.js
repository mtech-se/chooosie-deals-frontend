import * as PropTypes from 'prop-types';
import React from 'react';
import { Photo } from '../../models/Photo';
import { GalleryItem } from './GalleryItem';

export const Gallery = props => {
  const galleryItems = props.photos.map((photo, index, array) => {
    if (index === 4) return undefined;

    return (
      <GalleryItem
        key={photo.id}
        photo={photo}
        onGalleryItemClick={props.onGalleryItemClick}
        isActive={photo.id === props.selectedPhoto.id}
        isLast={index === array.length - 1}
      />
    );
  });

  return (
    <div style={{
      display: 'flex',
      // justifyContent: 'center',
      marginTop: '8px',
    }}>
      {galleryItems}
    </div>
  );
};

Gallery.propTypes = {
  selectedPhoto: PropTypes.instanceOf(Photo).isRequired,
  photos: PropTypes.arrayOf(PropTypes.instanceOf(Photo)).isRequired,
  onGalleryItemClick: PropTypes.func.isRequired,
};