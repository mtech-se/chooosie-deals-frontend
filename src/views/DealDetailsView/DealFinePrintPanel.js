import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Deal } from '../../models/Deal';
import { Section } from './Section';

export class DealFinePrintPanel extends Component {
  render() {
    return (
      <div {...this.props}>
        <Section
          title={'Fine Print'}
          content={(
            <div>

              <div style={{
                whiteSpace: 'pre-line',
                fontSize: '14px',
                marginTop: '16px',
                lineHeight: '21px',
              }}>{this.props.deal.termsOfUse}
              </div>

            </div>
          )}
          {...this.props}
        />
      </div>
    );
  }
}

DealFinePrintPanel.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
};