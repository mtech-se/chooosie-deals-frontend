import * as PropTypes from 'prop-types';
import React from 'react';

export const Section = props => (
  <div style={{
    backgroundColor: '#eeeeee',
    fontFamily: 'Libre Franklin',
    fontSize: '20px',
    color: '#4a4a4a',
    padding: '32px',
  }}>

    <div style={{
      fontWeight: '700',
      marginBottom: '32px',
    }}>
      {props.title}
    </div>

    <div>
      {props.content}
    </div>

  </div>
);

Section.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.element.isRequired,
};