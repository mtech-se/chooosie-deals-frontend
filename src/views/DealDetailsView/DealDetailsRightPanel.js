import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { RecommendedDealsPanel } from '../../components/RecommendedDealsPanel';
import { Deal } from '../../models/Deal';
import { DealDetailsActionPanel } from './DealDetailsActionPanel';

class BaseDealDetailsRightPanel extends Component {
  render() {
    return (
      <div style={{
        paddingLeft: '32px',
        paddingRight: '108px',
      }}>
        <DealDetailsActionPanel
          deal={this.props.deal}
        />

        <RecommendedDealsPanel
          deals={this.props.recommendedDeals}
          style={{
            marginTop: '32px',
          }}
        />
      </div>
    );
  }
}

BaseDealDetailsRightPanel.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
  recommendedDeals: PropTypes.arrayOf(PropTypes.instanceOf(Deal)),
};

export const DealDetailsRightPanel = withRouter(BaseDealDetailsRightPanel);