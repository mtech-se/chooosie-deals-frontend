import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Deal } from '../../models/Deal';
import { DealDescriptionPanel } from './DealDescriptionPanel';
import { DealFinePrintPanel } from './DealFinePrintPanel';
import { DealRedemptionInstructionsPanel } from './DealRedemptionInstructionsPanel';
import { Gallery } from './Gallery';
import { MainImageDisplay } from './MainImageDisplay';

export class DealDetailsLeftPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedPhoto: this.props.deal.entityPhotos.length === 0 ? undefined : this.props.deal.entityPhotos[0].photo,
      photos: this.props.deal.entityPhotos.map(dealPhoto => dealPhoto.photo),
    };

    this.onGalleryItemClick = this.onGalleryItemClick.bind(this);
    this.onMainImageScrollClick = this.onMainImageScrollClick.bind(this);
  }

  onMainImageScrollClick(direction) {
    let currentIndex = undefined;
    this.state.photos.forEach((photo, index) => {
      if (photo.id === this.state.selectedPhoto.id) {
        currentIndex = index;
      }
    });

    let newSelectedPhotoIndex = undefined;
    switch (direction) {
      case 'left':
        newSelectedPhotoIndex = currentIndex === 0 ? this.state.photos.length - 1 : currentIndex - 1;
        break;
      case 'right':
        newSelectedPhotoIndex = currentIndex === this.state.photos.length - 1 ? 0 : currentIndex + 1;
        break;
      default:
        throw new Error('Unknown direction');
    }

    this.setState({
      selectedPhoto: this.state.photos[newSelectedPhotoIndex],
    });
  }

  onGalleryItemClick(clickedPhoto) {
    this.state.photos.forEach(photo => {
      if (photo.id === clickedPhoto.id) {
        this.setState({
          selectedPhoto: photo,
        });
      }
    });
  }

  render() {
    return (
      <div style={{
        paddingLeft: '128px',
      }}>
        <MainImageDisplay
          photo={this.state.selectedPhoto}
          onMainImageScrollClick={this.onMainImageScrollClick}
          showSideScrollers={this.state.photos.length >= 2}
        />

        {this.state.photos.length !== 0 && (
          <Gallery
            selectedPhoto={this.state.selectedPhoto}
            photos={this.state.photos}
            onGalleryItemClick={this.onGalleryItemClick}
          />
        )}

        <DealDescriptionPanel
          style={{ marginTop: '16px' }}
          deal={this.props.deal}
        />

        <DealRedemptionInstructionsPanel
          style={{ marginTop: '16px' }}
          deal={this.props.deal}
        />

        <DealFinePrintPanel
          style={{ marginTop: '16px' }}
          deal={this.props.deal}
        />

      </div>
    );
  }
}

DealDetailsLeftPanel.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
};