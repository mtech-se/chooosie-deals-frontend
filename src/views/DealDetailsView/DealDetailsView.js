import { Grid } from '@material-ui/core/es/index';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import * as ReactRouterPropTypes from 'react-router-prop-types';
import { BaseProgressLoader } from '../../components/BaseProgressLoader';
import { DealFactory } from '../../factories/DealFactory';
import { DealService } from '../../services/DealService';
import { DealDetailsLeftPanel } from './DealDetailsLeftPanel';
import { DealDetailsRightPanel } from './DealDetailsRightPanel';

class BaseDealDetailsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasFetchedData: false,
      deal: undefined,
      recommendedDeals: undefined,
    };
  }

  async componentDidMount() {
    let response;

    response = await DealService.getDeal(this.props.match.params.dealId);
    const deal = DealFactory.createFromJson(response.data);

    response = await DealService.getRecommendedDeals(this.props.match.params.dealId);
    const recommendedDeals = DealFactory.createFromJsonArray(response.data.content);

    this.setState({
      hasFetchedData: true,
      deal,
      recommendedDeals,
    });
  }

  render() {
    if (this.state.hasFetchedData === false) return <BaseProgressLoader/>;

    return (
      <Grid container>

        {/*<Grid item xs={12}>*/}
        {/*<Breadcrumb sideMargin={'128px'}/>*/}
        {/*</Grid>*/}

        <Grid item
              xs={12}>
          <div style={{
            color: '#36173b',
            fontSize: '30px',
            fontFamily: 'Libre Franklin, sans-serif',
            fontWeight: '900',
            padding: '16px 128px',
            marginTop: '16px',
          }}>{this.state.deal.title}</div>
        </Grid>

        <Grid item
              xs={8}>
          <DealDetailsLeftPanel deal={this.state.deal}/>
        </Grid>

        <Grid item
              xs={4}>
          <DealDetailsRightPanel
            deal={this.state.deal}
            recommendedDeals={this.state.recommendedDeals}
          />
        </Grid>

      </Grid>
    );
  }
}

BaseDealDetailsView.propTypes = {
  match: ReactRouterPropTypes.match.isRequired
};

export const DealDetailsView = withRouter(BaseDealDetailsView);