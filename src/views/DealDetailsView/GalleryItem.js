import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { ApiConstants } from '../../constants/ApiConstants';
import { Photo } from '../../models/Photo';

export class GalleryItem extends Component {
  render() {
    const length = '189px';
    const opacity = this.props.isActive ? '1' : '0.8';

    return (
      <div
        onClick={() => this.props.onGalleryItemClick(this.props.photo)}
        style={{
          backgroundColor: 'black',
          cursor: 'pointer',
          marginRight: this.props.isLast ? '0' : '8.9px',
        }}
      >
        <img
          src={ApiConstants.BASE_URL + '/photos/' + this.props.photo.id}
          style={{
            width: length,
            height: length,
            objectFit: 'cover',
            opacity: opacity,
            display: 'block',
          }}
          alt=""
        />
      </div>
    );
  }
}

GalleryItem.propTypes = {
  photo: PropTypes.instanceOf(Photo).isRequired,
  onGalleryItemClick: PropTypes.func.isRequired,
  isActive: PropTypes.bool,
  isLast: PropTypes.bool,
};

GalleryItem.defaultProps = {
  isActive: false,
};