import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import placeholderHeroImage from '../../assets/images/782x367.png';
import { ApiConstants } from '../../constants/ApiConstants';
import { DimensionConstants } from '../../constants/DimensionConstants';
import { Photo } from '../../models/Photo';

export class MainImageDisplay extends Component {
  render() {
    const photoBaseUrl = ApiConstants.BASE_URL + '/photos/';
    const imageSrc = this.props.photo === undefined ? placeholderHeroImage : photoBaseUrl + this.props.photo.id;

    return (
      <div style={{
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
      }}>

        <img
          src={imageSrc}
          alt=""
          style={{
            width: DimensionConstants.dealGalleryMainImageWidth,
            height: DimensionConstants.dealGalleryMainImageHeight,
            objectFit: 'cover',
            display: 'block',
          }}/>

        {this.props.showSideScrollers && (
          <div
            onClick={() => this.props.onMainImageScrollClick('left')}
            style={{
              backgroundColor: 'rgba(0, 0, 0, 0.15)',
              position: 'absolute',
              top: '0',
              left: '0',
              height: '100%',
              cursor: 'pointer',
            }}>
            <ChevronLeft style={{
              fontSize: '80px',
              color: '#ffffff',
              height: '100%',
            }}/>
          </div>
        )}

        {this.props.showSideScrollers && (
          <div
            onClick={() => this.props.onMainImageScrollClick('right')}
            style={{
              backgroundColor: 'rgba(0, 0, 0, 0.15)',
              position: 'absolute',
              top: '0',
              right: '0',
              height: '100%',
              cursor: 'pointer',
            }}>
            <ChevronRight style={{
              fontSize: '80px',
              color: '#ffffff',
              height: '100%',
            }}/>
          </div>
        )}

      </div>
    );
  }
}

MainImageDisplay.propTypes = {
  photo: PropTypes.instanceOf(Photo),
  onMainImageScrollClick: PropTypes.func.isRequired,
  showSideScrollers: PropTypes.bool,
};