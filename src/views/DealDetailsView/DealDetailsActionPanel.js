import Dialog from '@material-ui/core/es/Dialog/Dialog';
import DialogTitle from '@material-ui/core/es/DialogTitle/DialogTitle';
import IconButton from '@material-ui/core/es/IconButton/IconButton';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { DiscountChip } from '../../components/DiscountChip';
import { OrderFactory } from '../../factories/OrderFactory';
import { Customer } from '../../models/Customer';
import { Deal } from '../../models/Deal';
import { MyOrderService } from '../../services/MyOrderService';
import { CurrencyUtils } from '../../utils/CurrencyUtils';
import { ActionButton } from './ActionButton';

export class BaseDealDetailsActionPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDisabled: true,
      isVisible: false,
      orderQuantity: 0,
      orderAmountPayable: '$0.00'
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.incrementQuantity = this.incrementQuantity.bind(this);
    this.decrementQuantity = this.decrementQuantity.bind(this);
    this.addToCart = this.addToCart.bind(this);
  }

  handleOpen() {
    this.hasCustomerLoggedIn();
    this.setState({ isVisible: true });
  }

  handleClose() {
    this.setState({ isVisible: false });
  }

  incrementQuantity() {
    const orderQuantity = this.state.orderQuantity + 1;

    this.setState({
      orderQuantity
    }, () => {
      this.checkOrderQuantity();
      this.calculateAmountPayable();
    });
  }

  decrementQuantity() {
    let orderQuantity = 0;

    if (this.state.orderQuantity > 0) orderQuantity = this.state.orderQuantity - 1;

    this.setState({
      orderQuantity
    }, () => {
      this.checkOrderQuantity();
      this.calculateAmountPayable();
    });
  }

  calculateAmountPayable() {
    const orderAmountPayable = CurrencyUtils.toCurrency((this.state.orderQuantity * this.props.deal.discountedPrice) / 100);

    this.setState({ orderAmountPayable });
  }

  checkOrderQuantity() {
    if (this.state.orderQuantity !== 0) {
      this.setState({ isDisabled: false });
    }
  }

  hasCustomerLoggedIn() {
    if (!this.props.customer) {
      this.props.history.push('/login');
    }
  }

  addToCart() {
    this.setState({
      disableButton: true,
    }, async () => {
      const response = await MyOrderService.getCurrentOrder();

      let responseCurrentOrder = undefined;

      if (response.data.content[0] === undefined) {
        const responseNewOrder = await MyOrderService.createNewOrder();
        responseCurrentOrder = OrderFactory.createFromJson(responseNewOrder.data);
      } else {
        responseCurrentOrder = OrderFactory.createFromJson(response.data.content[0]);
      }

      let orderItems = [];
      for (let i = 0; i < this.state.orderQuantity; i++) {
        orderItems.push({
            'deal': {
              'id': this.props.deal.id
            }
          }
        );
      }

      let requestBody = undefined;

      if (responseCurrentOrder.orderItems) {
        requestBody = {
          'order_items': orderItems.concat(responseCurrentOrder.orderItems)
        };
      } else {
        requestBody = {
          'order_items': orderItems
        };
      }

      const responseUpdateOrder = await MyOrderService.updateOrder(responseCurrentOrder.id, requestBody);

      if (responseUpdateOrder.status === 200) {
        alert('Order added to cart.');
        this.handleClose();

        this.setState({ orderQuantity: 0 });
      } else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  render() {
    return (
      <div style={{
        backgroundColor: '#eeeeee',
        padding: '32px 24px',
        fontFamily: 'Libre Franklin, sans-serif',
      }}>

        <div style={{
          display: 'flex',
          justifyContent: 'space-between',
          padding: '0 16px',
        }}>
          <div>
            <div style={{
              fontSize: '24px',
              fontWeight: '500',
              textDecoration: 'line-through',
              color: '#4a4a4a',
              marginRight: '4px',
            }}>{this.props.deal.getOriginalPrice()}
            </div>
            <div style={{
              fontSize: '24px',
              fontWeight: '700',
              color: '#417505',
              marginTop: '4px',
            }}>{this.props.deal.getDiscountedPrice()}
            </div>
          </div>
          <div style={{
            textAlign: 'center',
          }}>
            <div>
              <DiscountChip label={this.props.deal.getDiscountPercentage() + ' off'}/>
            </div>
            <div style={{ marginTop: '8px' }}>
              <span style={{
                fontSize: '12px',
                fontWeight: '700',
                color: '#4a4a4a',
              }}>{this.props.deal.numBought === null ? '0' : this.props.deal.numBought} bought</span>
            </div>
          </div>
        </div>

        <div style={{
          marginTop: '24px',
        }}>
          <ActionButton
            label={'BUY NOW'}
            width={'100%'}
            color={'#eeeeee'}
            backgroundColor={'#601e59'}
            fontSize={'18px'}
            onClick={this.handleOpen}
          />
        </div>

        <Dialog
          open={this.state.isVisible}
          onClose={this.handleClose}
        >
          <DialogTitle>{this.props.deal.title}</DialogTitle>
          <div>
            <div
              style={{
                display: 'flex',
                width: '500px',
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: '8px'
              }}>
              <div
                style={{
                  marginRight: '8px'
                }}>
                Quantity:
              </div>

              <div
                style={{
                  marginRight: '8px'
                }}>
                <IconButton
                  onClick={this.decrementQuantity}
                >
                  <RemoveCircleOutlineIcon/>
                </IconButton>
              </div>

              <div
                style={{
                  marginRight: '8px'
                }}>
                {this.state.orderQuantity}
              </div>

              <div>
                <IconButton
                  onClick={this.incrementQuantity}
                >
                  <AddCircleOutlineIcon/>
                </IconButton>
              </div>
            </div>

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: '8px'
              }}>
              <div
                style={{
                  marginRight: '8px'
                }}>
                Amount Payable:
              </div>

              <div
                style={{
                  marginRight: '8px'
                }}>
                <div style={{
                  fontSize: '16px',
                  fontWeight: 'bold',
                }}>
                  {this.state.orderAmountPayable}
                </div>
              </div>
            </div>

            <div style={{
              padding: '32px 24px'
            }}>
              <ActionButton
                label={'Add To Cart'}
                width={'100%'}
                color={'#eeeeee'}
                backgroundColor={'#601e59'}
                fontSize={'18px'}
                disabled={this.state.isDisabled}
                onClick={this.addToCart}
              />
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

BaseDealDetailsActionPanel.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
  deal: PropTypes.instanceOf(Deal).isRequired,
  customer: PropTypes.instanceOf(Customer),
};

const mapStateToProps = state => ({
  customer: state.customer,
});

export const DealDetailsActionPanel = connect(mapStateToProps)(withRouter(BaseDealDetailsActionPanel));