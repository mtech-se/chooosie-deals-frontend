import { Button } from '@material-ui/core/es/index';
import withStyles from '@material-ui/core/es/styles/withStyles';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';

const styles = {};

class BaseActionButton extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Button
        variant="contained"
        className={classes.button}
        style={{
          width: this.props.width,
          color: this.props.color,
          backgroundColor: this.props.disabled ? 'grey' : this.props.backgroundColor,
          fontSize: this.props.fontSize,
          fontFamily: 'Libre Franklin, san-serif',
        }}
        disabled={this.props.disabled}
        onClick={this.props.onClick}
      >
        {this.props.label}
      </Button>
    );
  }
}

BaseActionButton.propTypes = {
  label: PropTypes.string.isRequired,
  width: PropTypes.string,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  fontSize: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  classes: PropTypes.object,
};

export const ActionButton = withStyles(styles)(BaseActionButton);

ActionButton.propTypes = {
  label: PropTypes.string.isRequired,
  width: PropTypes.string,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  fontSize: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

ActionButton.defaultProps = {
  width: 'initial',
  color: '#222222',
  backgroundColor: '#eeeeee',
  fontSize: 'initial',
  disabled: false
};