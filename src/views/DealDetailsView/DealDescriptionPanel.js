import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Deal } from '../../models/Deal';
import { Section } from './Section';

export class DealDescriptionPanel extends Component {
  render() {
    return (
      <div {...this.props}>
        <Section
          title={'Description'}
          content={(
            <div style={{
              whiteSpace: 'pre-line',
              fontSize: '14px',
              lineHeight: '21px',
            }}>
              {this.props.deal.description}
            </div>
          )}
          {...this.props}
        />
      </div>
    );
  }
}

DealDescriptionPanel.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
};