import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import placeholderBrandImage from '../../assets/images/48x48.png';
import { BrandAvatar } from '../../components/BrandAvatar';
import { Deal } from '../../models/Deal';
import { Section } from './Section';

export class DealRedemptionInstructionsPanel extends Component {
  render() {
    const imageUrl = this.props.deal.outlet.brand.getMainPhotoUrl() === undefined ? placeholderBrandImage : this.props.deal.outlet.brand.getMainPhotoUrl();

    return (
      <div {...this.props}>
        <Section
          title={'Redemption Details'}
          content={(
            <div>

              <div style={{ display: 'flex', alignItems: 'center' }}>
                <BrandAvatar
                  image={imageUrl}
                  size={'48px'}
                />
                <div style={{
                  fontSize: '14px',
                  marginLeft: '16px',
                  lineHeight: '20px',
                }}>
                  <div style={{
                    fontWeight: '700'
                  }}>{this.props.deal.outlet.brand.name}
                  </div>
                  <div>{this.props.deal.address}</div>
                </div>
              </div>

              <div style={{
                whiteSpace: 'pre-line',
                fontSize: '14px',
                marginTop: '16px',
                lineHeight: '21px',
              }}>{this.props.deal.redemptionInstructions}
              </div>

            </div>
          )}
          {...this.props}
        />
      </div>
    );
  }
}

DealRedemptionInstructionsPanel.propTypes = {
  deal: PropTypes.instanceOf(Deal).isRequired,
};