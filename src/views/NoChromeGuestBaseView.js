import * as PropTypes from 'prop-types';
import React from 'react';

export class NoChromeGuestBaseView extends React.Component {
  render() {
    return (
      <div style={{
        margin: '0 auto',
        maxWidth: '1366px',
        minHeight: '768px',
        display: 'flex',
        alignItems: 'center',
      }}>
        {this.props.component}
      </div>
    );
  }
}

NoChromeGuestBaseView.propTypes = {
  component: PropTypes.element.isRequired,
};