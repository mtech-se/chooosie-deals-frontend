import { shallow } from 'enzyme';
import React from 'react';
import { RegisterView } from './RegisterView';

describe('RegisterView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<RegisterView/>);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});