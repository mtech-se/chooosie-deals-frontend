import FormControl from '@material-ui/core/es/FormControl/FormControl';
import FormControlLabel from '@material-ui/core/es/FormControlLabel/FormControlLabel';
import FormLabel from '@material-ui/core/es/FormLabel/FormLabel';
import Radio from '@material-ui/core/es/Radio/Radio';
import RadioGroup from '@material-ui/core/es/RadioGroup/RadioGroup';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { AppBrand } from '../../components/AppBrand/AppBrand';
import { BaseProgressLoader } from '../../components/BaseProgressLoader';
import { LabelField } from '../../components/LabelField/LabelField';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { CustomerService } from '../../services/CustomerService';

export class BaseRegisterView extends Component {
  constructor(state) {
    super(state);

    this.state = {
      isMakingNetworkCall: false,
      isRegisterButtonDisabled: false,
      customerFirstName: 'hello',
      customerLastName: 'customer',
      customerEmail: 'customer@customer.sg',
      customerPassword: 'secret',
      customerGender: 'female',
      customerBillingFirstName: 'hello',
      customerBillingLastName: 'customer',
      customerBillingCellphone: '90246810',
      customerBillingTelephone: '60248102',
      customerBillingAddressLineOne: '25 Heng Mui Keng Terrace',
      customerBillingAddressLineTwo: '#03-03',
      customerBillingAddressPostalCode: '119615',
    };

    this.onRegisterButtonClick = this.onRegisterButtonClick.bind(this);
  }

  async onRegisterButtonClick() {
    this.setState({ isMakingNetworkCall: true }, async () => {

      const requestBody = {
        'email': this.state.customerEmail,
        'first_name': this.state.customerFirstName,
        'last_name': this.state.customerLastName,
        'gender': this.state.customerGender,
        'password': this.state.customerPassword,
        'billing_first_name': this.state.customerBillingFirstName,
        'billing_last_name': this.state.customerBillingLastName,
        'billing_cellphone': this.state.customerBillingCellphone,
        'billing_telephone': this.state.customerBillingTelephone,
        'billing_add_line_one': this.state.customerBillingAddressLineOne,
        'billing_add_line_two': this.state.customerBillingAddressLineTwo,
        'billing_add_postal_code': this.state.customerBillingAddressPostalCode,
      };

      const response = await CustomerService.createCustomer(requestBody);

      console.log(response);

      if (response.status === 201) {
        alert('Your account has been created. Login to continue.');
        this.props.history.push('/login');
      }
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  render() {
    const content = this.state.isRegisterButtonDisabled ? <BaseProgressLoader loadingMessage={'Registering...'}/> : (

      <div style={{
        width: '1000px',
      }}>
        <AppBrand
          imageHeight={'56px'}
          imageMargin={'8px'}
          style={{
            borderRadius: '8px 8px 0 0',
          }}
        />

        <div style={{
          padding: '16px',
          borderRadius: '0 0 8px 8px',
          backgroundColor: ColorConstants.faintGray,
        }}>

          <div
            style={{
              justifyContent: 'center'
            }}
          >
            <PanelTitle
              title={'Join us?'}
              marginBottom={'16px'}
            />
          </div>

          <div>
            <div style={{
              display: 'flex',
              flexDirection: 'row',
              marginBottom: '16px'
            }}>
              <div style={{
                width: '50%',
                marginRight: '8px',
              }}>
                <div style={{
                  width: '90%'
                }}>
                  <div
                    style={{
                      marginBottom: '8px',
                    }}>
                    Account Information
                  </div>

                  <LabelField
                    title={'First Name'}
                    type={'text'}
                    value={this.state.customerFirstName}
                    onChangeHandler={event => {
                      this.setState({
                        customerFirstName: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Last Name'}
                    type={'text'}
                    value={this.state.customerLastName}
                    onChangeHandler={event => {
                      this.setState({
                        customerLastName: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Email'}
                    type={'email'}
                    value={this.state.customerEmail}
                    onChangeHandler={event => {
                      this.setState({
                        customerEmail: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Password'}
                    type={'password'}
                    value={this.state.customerPassword}
                    onChangeHandler={event => {
                      this.setState({
                        customerPassword: event.target.value
                      });
                    }}
                    required={true}
                  />
                </div>

                <div
                  style={{
                    marginTop: '16px'
                  }}>
                  <FormControl>
                    <FormLabel component='legend'>Gender</FormLabel>
                    <RadioGroup
                      value={this.state.customerGender}
                      onChange={event => this.setState({ customerGender: event.target.value })}
                    >
                      <FormControlLabel
                        value='female'
                        control={<Radio color='primary'/>}
                        label='Female'
                      />
                      <FormControlLabel
                        value='male'
                        control={<Radio color='primary'/>}
                        label='Male'
                      />
                    </RadioGroup>
                  </FormControl>
                </div>
              </div>

              <div style={{
                width: '50%',
                marginBottom: '8px',
              }}>
                <div style={{
                  width: '90%'
                }}>
                  <div
                    style={{
                      marginBottom: '8px',
                    }}>
                    Billing Information
                  </div>

                  <LabelField
                    title={'First Name'}
                    type={'text'}
                    value={this.state.customerBillingFirstName}
                    onChangeHandler={event => {
                      this.setState({
                        customerBillingFirstName: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Last Name'}
                    type={'text'}
                    value={this.state.customerBillingLastName}
                    onChangeHandler={event => {
                      this.setState({
                        customerBillingLastName: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Cellphone Number'}
                    type={'text'}
                    value={this.state.customerBillingCellphone}
                    onChangeHandler={event => {
                      this.setState({
                        customerBillingCellphone: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Telephone Number'}
                    type={'text'}
                    value={this.state.customerBillingTelephone}
                    onChangeHandler={event => {
                      this.setState({
                        customerBillingTelephone: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Address (Line 1)'}
                    type={'text'}
                    value={this.state.customerBillingAddressLineOne}
                    onChangeHandler={event => {
                      this.setState({
                        customerBillingAddressLineOne: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Address (Line 2)'}
                    type={'text'}
                    value={this.state.customerBillingAddressLineTwo}
                    onChangeHandler={event => {
                      this.setState({
                        customerBillingAddressLineTwo: event.target.value
                      });
                    }}
                    required={true}
                  />

                  <LabelField
                    title={'Postal Code'}
                    type={'text'}
                    value={this.state.customerBillingAddressPostalCode}
                    onChangeHandler={event => {
                      this.setState({
                        customerBillingAddressPostalCode: event.target.value
                      });
                    }}
                    required={true}
                  />
                </div>
              </div>
            </div>
          </div>

          <div style={{
            marginTop: '16px',
          }}>
            <StylisedButton
              title={'Register'}
              onClick={this.onRegisterButtonClick}
              disabled={this.state.isRegisterButtonDisabled}
              colour={ColorConstants.teal}
            />
          </div>
        </div>
      </div>
    );

    return (
      <div style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        {content}
      </div>
    );
  }
}

BaseRegisterView.propTypes = {
  history: PropTypes.object,
};

export const RegisterView = withRouter(BaseRegisterView);