import * as PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { clearAppData } from '../../store/actions/action-creators';

const BaseLogoutView = props => {
  props.clearAppData();

  return <Redirect to={'/login'}/>;
};

BaseLogoutView.propTypes = {
  clearAppData: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    clearAppData: () => dispatch(clearAppData()),
  };
};

export const LogoutView = connect(null, mapDispatchToProps)(BaseLogoutView);