import Tab from '@material-ui/core/es/Tab/Tab';
import Tabs from '@material-ui/core/es/Tabs/Tabs';
import React, { Component } from 'react';
import { MyAccountDetails } from '../../components/MyAccountDetails/MyAccountDetails';
import { MyOrderHistory } from '../../components/MyOrderHistory/MyOrderHistory';
import { TabContainer } from '../../components/TabContainer/TabContainer';

export class MyAccountView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tabValue: 0,
    };
  }

  handleChange = (event, value) => {
    this.setState({ tabValue: value });
  };

  render() {
    return (
      <div>
        <div style={{
          padding: '16px 100px 10px',
        }}
        >
          <Tabs
            style={{
              marginBottom: '8px'
            }}
            value={this.state.tabValue}
            onChange={this.handleChange}
            textColor="primary"
            indicatorColor='primary'
            variant='fullWidth'
          >
            <Tab
              focusRipple={true}
              label="ACCOUNT DETAILS"
            />
            <Tab
              focusRipple={true}
              label="ORDER HISTORY"
            />
          </Tabs>

          {this.state.tabValue === 0 &&
          <TabContainer>
            <MyAccountDetails/>
          </TabContainer>}

          {this.state.tabValue === 1 &&
          <TabContainer>
            <MyOrderHistory/>
          </TabContainer>}
        </div>
      </div>
    );
  }
}