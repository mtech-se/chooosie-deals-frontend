import { Grid } from '@material-ui/core/es/index';
import React from 'react';
import { AllDeals } from '../../components/AllDeals';
import { CategoryDeals } from '../../components/CategoryDeals/CategoryDeals';
import { DealsOfTheMonth } from '../../components/DealsOfTheMonth/DealsOfTheMonth';

export const HomeView = () => {
  return (
    <Grid container
          style={{ marginTop: '16px' }}>

      <Grid item
            xs={12}
            style={{ marginTop: '40px' }}>
        <DealsOfTheMonth/>
      </Grid>

      {/*<Grid item*/}
      {/*xs={12}*/}
      {/*style={{ marginTop: '40px' }}>*/}
      {/*<RecommendedDeals/>*/}
      {/*</Grid>*/}

      <Grid item
            xs={12}
            style={{ marginTop: '40px' }}>
        <AllDeals/>
      </Grid>

      <Grid item
            xs={12}
            style={{ marginTop: '40px' }}>
        <CategoryDeals/>
      </Grid>

    </Grid>
  );
};