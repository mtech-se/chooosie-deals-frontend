import React from 'react';
import { BaseProgressLoader } from '../../components/BaseProgressLoader';
import { BillingDetailsPanel } from '../../components/BillingDetailsPanel/BillingDetailsPanel';
import { ShoppingCartPanel } from '../../components/ShoppingCartPanel/ShoppingCartPanel';
import { ColorConstants } from '../../constants/ColorConstants';
import { OrderFactory } from '../../factories/OrderFactory';
import { MyOrderService } from '../../services/MyOrderService';

export class CheckoutView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabValue: 0,
      currentOrder: undefined,
      isMakingNetworkCall: false
    };
  }

  componentDidMount() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await MyOrderService.getCurrentOrder();
      const currentOrder = OrderFactory.createFromJson(response.data.content[0]);

      this.setState({
        currentOrder,
        isMakingNetworkCall: false,
      });
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return (
      <BaseProgressLoader
        style={{
          marginTop: '64px',
        }}
      />
    );

    if (this.state.currentOrder === undefined || this.state.currentOrder.orderItems.length === 0) return (
      <div style={{
        textAlign: 'center',
        lineHeight: '350px',
        height: '350px'
      }}>
          <span style={{
            display: 'inline-block',
            verticalAlign: 'middle',
            color: ColorConstants.secondaryPurple,
            fontSize: '16',
            fontWeight: 'bold'
          }}>
            No items found in cart. Add items first.
          </span>
      </div>
    );

    return (
      <div>
        <div style={{
          height: '75px',
          backgroundColor: '#ebebeb',
          textAlign: 'center',
          lineHeight: '75px'
        }}>
          <span style={{
            display: 'inline-block',
            verticalAlign: 'middle',
            color: ColorConstants.secondaryPurple,
            fontSize: '16',
            fontWeight: 'bold'
          }}>
            CHECKOUT
          </span>
        </div>

        <div
          style={{
            display: 'flex'
          }}
        >
          <div style={{
            flexGrow: '2',
            padding: '40px 40px 25px',
          }}
          >
            <BillingDetailsPanel
              currentOrder={this.state.currentOrder}
              setIsMakingNetworkCall={isMakingNetworkCall => this.setState({ isMakingNetworkCall })}
            />
          </div>

          <div style={{
            flexGrow: '1',
            padding: '40px 40px 25px',
            backgroundColor: '#f6f6f6'
          }}
          >
            <ShoppingCartPanel
              currentOrder={this.state.currentOrder}
            />
          </div>
        </div>
      </div>
    );
  }
}