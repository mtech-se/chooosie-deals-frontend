import { Grid } from '@material-ui/core/es';
import * as PropTypes from 'prop-types';
import React from 'react';
import { Footer } from '../components/Footer/Footer';
import { TopNavBar } from '../components/TopNavBar/TopNavBar';

export class GuestBaseView extends React.Component {
  render() {
    return (
      <div style={{
        margin: '0 auto',
        maxWidth: '1366px',
        minHeight: '768px',
        backgroundColor: '#ffffff',
      }}>
        <Grid container>

          <Grid item
                xs={12}>
            <TopNavBar/>
          </Grid>

          <Grid item
                xs={12}>
            {this.props.component}
          </Grid>

          <Grid item
                xs={12}
                style={{ marginTop: '64px' }}>
            <Footer/>
          </Grid>

        </Grid>
      </div>
    );
  }
}

GuestBaseView.propTypes = {
  component: PropTypes.element.isRequired,
};