import { Grid } from '@material-ui/core/es/index';
import { withStyles } from '@material-ui/core/styles';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Footer } from '../components/Footer/Footer';
import { TopNavBar } from '../components/TopNavBar/TopNavBar';
import { initStoreWithAccessTokenAndUpdateCustomerDetails } from '../store/actions/action-creators';

class BaseAuthBaseView extends Component {
  componentDidMount() {
    this.props.initStore();
  }

  render() {
    if (this.props.accessToken === undefined) return <div>Waiting for Redux to hydrate...</div>;

    return (
      <div style={{
        margin: '0 auto',
        maxWidth: '1366px',
        backgroundColor: '#ffffff',
        minHeight: '768px',
      }}>
        <Grid container>

          <Grid item
                xs={12}>
            <TopNavBar/>
          </Grid>

          <Grid item
                xs={12}>
            {this.props.component}
          </Grid>

          <Grid item
                xs={12}
                style={{ marginTop: '64px' }}>
            <Footer/>
          </Grid>

        </Grid>
      </div>
    );
  }
}

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

BaseAuthBaseView.propTypes = {
  classes: PropTypes.object.isRequired,
  component: PropTypes.element,
  accessToken: PropTypes.string,
  initStore: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  accessToken: state.accessToken,
});

const mapDispatchToProps = dispatch => {
  return {
    initStore: () => dispatch(initStoreWithAccessTokenAndUpdateCustomerDetails()),
  };
};

export const AuthBaseView = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(BaseAuthBaseView));
