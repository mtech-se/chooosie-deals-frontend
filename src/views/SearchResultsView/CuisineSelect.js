import * as PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { FilterSelect } from '../../components/FilterSelect';
import { CuisineFactory } from '../../factories/CuisineFactory';
import { CuisineService } from '../../services/CuisineService';

class BaseCuisineSelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasFetchedData: false,
      options: [],
      selectedOption: undefined,
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(element) {
    this.setState({
      selectedOption: element,
    });

    this.props.onChange(element);
  }

  async componentDidMount() {
    const response = await CuisineService.getCuisines();
    const cuisines = CuisineFactory.createFromJsonArray(response.data.content);
    const options = cuisines.map(cuisine => {
      return { value: cuisine.id, label: cuisine.name };
    });

    const cuisineId = queryString.parse(this.props.location.search).cuisineId;
    const selectedOption = options.find(option => option.value.toString() === cuisineId);

    this.setState({
      hasFetchedData: true,
      options,
      selectedOption,
    });
  }

  render() {
    return (
      <FilterSelect
        label={'Cuisine'}
        options={this.state.options}
        value={this.state.selectedOption}
        onChange={this.onChange}
        style={{ ...this.props.style }}
        isDisabled={!this.state.hasFetchedData}
      />
    );
  }
}

BaseCuisineSelect.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
  style: PropTypes.object,
  onChange: PropTypes.func.isRequired,
};

export const CuisineSelect = withRouter(BaseCuisineSelect);

CuisineSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
};