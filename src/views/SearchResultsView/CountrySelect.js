import * as PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { FilterSelect } from '../../components/FilterSelect';
import { CountryFactory } from '../../factories/CountryFactory';
import { CountryService } from '../../services/CountryService';

class BaseCountrySelect extends Component {
  constructor(props) {
    super(props);

    const options = [
      { value: 1, label: 'Singapore' },
      { value: 2, label: 'Malaysia' },
      { value: 3, label: 'Indonesia' },
    ];

    const countryId = queryString.parse(this.props.location.search).countryId;
    const selectedOption = options.find(option => option.value === Number.parseInt(countryId, 10));

    this.state = {
      options,
      selectedOption,
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(element) {
    this.setState({
      selectedOption: element,
    });

    this.props.onChange(element);
  }

  async componentDidMount() {
    const response = await CountryService.getCountries();
    const countries = CountryFactory.createCountries(response.data.content);
    const options = countries.map(location => {
      return { value: location.id, label: location.name };
    });

    const countryId = queryString.parse(this.props.location.search).countryId;
    const selectedOption = options.find(option => option.value.toString() === countryId);

    this.setState({
      hasFetchedData: true,
      options,
      selectedOption,
    });
  }

  render() {
    return (
      <FilterSelect
        label={'Country'}
        options={this.state.options}
        value={this.state.selectedOption}
        onChange={this.onChange}
        style={{ ...this.props.style }}
        // isDisabled={!this.state.hasFetchedData}
      />
    );
  }
}

BaseCountrySelect.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
  style: PropTypes.object,
  onChange: PropTypes.func.isRequired,
};

export const CountrySelect = withRouter(BaseCountrySelect);

CountrySelect.propTypes = {
  onChange: PropTypes.func.isRequired,
};