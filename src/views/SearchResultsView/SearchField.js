import * as PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';

class BaseSearchField extends Component {
  constructor(props) {
    super(props);

    const searchTerm = queryString.parse(this.props.location.search).searchTerm;

    this.state = {
      searchValue: searchTerm === undefined ? '' : searchTerm,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(event) {
    this.setState({
      searchValue: event.target.value,
    });

    // this.props.onChange(event.target.value);
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.onChange(this.state.searchValue);
  }

  render() {
    return (
      <div
        style={{ ...this.props.style }}
      >
        <div style={{
          fontSize: '12px',
          fontFamily: 'Libre Franklin, sans-serif',
          fontWeight: '300',
          marginBottom: '4px',
        }}>{this.props.label}
        </div>
        <form onSubmit={event => this.onSubmit(event)}>
          <input
            type="text"
            value={this.state.searchValue}
            style={{
              borderRadius: '4px',
              border: '1px solid hsl(0,0%,80%)',
              width: '100%',
              height: '38px',
              padding: '0 12px',
              fontFamily: 'HelveticaNeue, sans-serif',
              fontSize: '14px',
              color: '#9b9b9b',
              backgroundColor: 'hsl(0,0%,98%)',
            }}
            onChange={event => this.onChange(event)}
          />
        </form>
      </div>
    );
  }
}

BaseSearchField.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
  style: PropTypes.object,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

export const SearchField = withRouter(BaseSearchField);

SearchField.propTypes = {
  label: PropTypes.string.isRequired,
};