import * as PropTypes from 'prop-types';
import queryString from 'query-string';
import Slider from 'rc-slider';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { StyleConstants } from '../../constants/StyleConstants';
import { CurrencyUtils } from '../../utils/CurrencyUtils';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

const valueStyle = {
  minWidth: '70px',
  textAlign: 'center',
  fontSize: 'initial',
  fontWeight: '300',
  fontFamily: StyleConstants.fontFamily,
  color: ColorConstants.softBlack,
  margin: '0 8px',
};

class BasePriceSelect extends Component {
  constructor(props) {
    super(props);

    const queryParams = queryString.parse(this.props.location.search);
    const priceBegin = queryParams.price_min === undefined ? this.props.rangeMin : queryParams.price_min / 100;
    const priceEnd = queryParams.price_max === undefined ? this.props.rangeMax : queryParams.price_max / 100;

    this.state = {
      selectedMin: priceBegin,
      selectedMax: priceEnd,

      lastKnownBeforeChangeMin: undefined,
      lastKnownBeforeChangeMax: undefined,

      lastKnownAfterChangeMin: undefined,
      lastKnownAfterChangeMax: undefined,
    };

    this.onBeforeChange = this.onBeforeChange.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onAfterChange = this.onAfterChange.bind(this);
  }

  onBeforeChange(values) {
    const [selectedMin, selectedMax] = values;

    if (selectedMin === this.state.lastKnownBeforeChangeMin && selectedMax === this.state.lastKnownBeforeChangeMax) return;

    this.setState({
      lastKnownBeforeChangeMin: selectedMin,
      lastKnownBeforeChangeMax: selectedMax,
    });
  }

  onChange(newValues) {
    const [selectedMin, selectedMax] = newValues;

    this.setState({
      selectedMin,
      selectedMax,
    });
  }

  onAfterChange(values) {
    const [selectedMin, selectedMax] = values;

    if (selectedMin === this.state.lastKnownAfterChangeMin && selectedMax === this.state.lastKnownAfterChangeMax) return;

    this.setState({
      lastKnownAfterChangeMin: selectedMin,
      lastKnownAfterChangeMax: selectedMax,
    });

    this.props.onChange([
      this.state.selectedMin,
      this.state.selectedMax
    ]);
  }

  render() {
    return (
      <div style={{
        width: '100%',
        display: 'flex',
      }}>
        <div style={valueStyle}>{CurrencyUtils.toCurrency(this.state.selectedMin)}</div>
        <Range
          min={this.props.rangeMin}
          max={this.props.rangeMax}
          dots={false}
          handleStyle={[{
            backgroundColor: ColorConstants.white,
            borderColor: ColorConstants.primaryPurple,
          }]}
          trackStyle={[{
            backgroundColor: ColorConstants.primaryPurple,
          }]}
          railStyle={{
            backgroundColor: ColorConstants.neutralOffWhite,
          }}
          dotStyle={{
            backgroundColor: ColorConstants.neutralOffWhite,
            borderColor: ColorConstants.primaryPurple,
          }}
          activeDotStyle={{
            backgroundColor: ColorConstants.primaryPurple,
            borderColor: ColorConstants.primaryPurple,
          }}
          defaultValue={[this.props.rangeMin, this.props.rangeMax]}
          allowCross={false}
          value={[this.state.selectedMin, this.state.selectedMax]}
          onBeforeChange={this.onBeforeChange}
          onChange={this.onChange}
          onAfterChange={this.onAfterChange}
          step={0.01}
        />
        <div style={valueStyle}>{CurrencyUtils.toCurrency(this.state.selectedMax)}</div>
      </div>
    );
  }
}

BasePriceSelect.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
  rangeMin: PropTypes.number.isRequired,
  rangeMax: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

export const PriceSelect = withRouter(BasePriceSelect);

PriceSelect.propTypes = {
  rangeMin: PropTypes.number.isRequired,
  rangeMax: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};