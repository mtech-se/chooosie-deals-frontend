import * as PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { FilterSelect } from '../../components/FilterSelect';
import { LocationFactory } from '../../factories/LocationFactory';
import { LocationService } from '../../services/LocationService';

class BaseLocationSelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasFetchedData: false,
      options: [],
      selectedOption: undefined,
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(element) {
    this.setState({
      selectedOption: element,
    });

    this.props.onChange(element);
  }

  async componentDidMount() {
    const response = await LocationService.getLocations();
    const locations = LocationFactory.createLocations(response.data.content);
    const options = locations.map(location => {
      return { value: location.id, label: location.name };
    });

    const locationId = queryString.parse(this.props.location.search).locationId;
    const selectedOption = options.find(option => option.value.toString() === locationId);

    this.setState({
      hasFetchedData: true,
      options,
      selectedOption,
    });
  }

  render() {
    return (
      <FilterSelect
        label={'Location'}
        options={this.state.options}
        value={this.state.selectedOption}
        onChange={this.onChange}
        style={{ ...this.props.style }}
        isDisabled={!this.state.hasFetchedData}
      />
    );
  }
}

BaseLocationSelect.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object,
};

export const LocationSelect = withRouter(BaseLocationSelect);

LocationSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
};