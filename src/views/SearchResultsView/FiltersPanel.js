import debounce from 'debounce';
import * as PropTypes from 'prop-types';
import 'rc-slider/assets/index.css';
import React, { Component } from 'react';
import { ColorConstants } from '../../constants/ColorConstants';
import { SearchParams } from '../../models/SearchParams';
import { PriceSelect } from './PriceSelect';

export class FiltersPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: this.props.searchTerm,
      // countryId: this.props.countryId,
      // locationId: this.props.locationId,
      // cuisineId: this.props.cuisineId,
      priceMin: this.props.priceMin,
      priceMax: this.props.priceMax,
    };

    this.onChange = this.onChange.bind(this);
    this.onChange = debounce(this.onChange, 200);
  }

  onChange(newValues) {
    const [selectedMin, selectedMax] = newValues;

    this.setState({
      priceMin: selectedMin * 100,
      priceMax: selectedMax * 100,
    }, () => {
      const searchParams = new SearchParams();
      searchParams.searchTerm = this.state.searchTerm;
      // searchParams.countryId = this.state.countryId;
      // searchParams.locationId = this.state.locationId;
      // searchParams.cuisineId = this.state.cuisineId;
      searchParams.priceMin = this.state.priceMin;
      searchParams.priceMax = this.state.priceMax;

      this.props.onChange(searchParams);
    });
  }

  render() {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}>
        <div style={{
          display: 'flex',
          justifyContent: 'center',
        }}>
          {/*<div style={{ marginRight: '32px' }}>*/}
          {/*<SearchField*/}
          {/*label={'Keywords'}*/}
          {/*style={{ marginBottom: '16px' }}*/}
          {/*onChange={newValue => this.setState({ searchTerm: newValue }, this.onChange)}*/}
          {/*/>*/}
          {/*</div>*/}
          {/*<div>*/}
          {/*<CuisineSelect*/}
          {/*onChange={newValue => {*/}
          {/*this.setState({*/}
          {/*cuisineId: newValue === null ? undefined : newValue.value,*/}
          {/*}, this.onChange);*/}
          {/*}}*/}
          {/*/>*/}
          {/*</div>*/}
        </div>

        <div style={{
          marginTop: '16px',
        }}>
          <div style={{
            fontSize: '12px',
            fontFamily: 'Libre Franklin, sans-serif',
            fontWeight: '300',
          }}>Price Range
          </div>
          <div style={{
            width: '592px',
            marginTop: '4px',
            padding: '24px 0',
            backgroundColor: ColorConstants.faintGray,
            borderColor: ColorConstants.transparentBlack,
            borderWidth: '1px',
            borderStyle: 'solid',
            borderRadius: '4px',
          }}>
            <PriceSelect
              rangeMin={0.00}
              rangeMax={100.00}
              onChange={this.onChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

FiltersPanel.propTypes = {
  onChange: PropTypes.func.isRequired,
  searchTerm: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  countryId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  locationId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  cuisineId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  priceMin: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  priceMax: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};