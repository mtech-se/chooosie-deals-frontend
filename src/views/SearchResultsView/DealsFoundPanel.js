import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { DealCardLandscapeWide } from '../../components/DealCard/DealCardLandscapeWide';
import { SectionTitle } from '../../components/SectionTitle';
import { Deal } from '../../models/Deal';

export class DealsFoundPanel extends Component {
  render() {
    const items = this.props.deals.map(deal => {
      return (
        <DealCardLandscapeWide
          key={deal.id}
          bottomMargin={'8px'}
          deal={deal}
        />
      );
    });

    const hitsLabel = this.props.deals.length === 1 ? 'deal' : 'deals';

    return (
      <div>
        <div style={{ marginBottom: '16px' }}>
          <SectionTitle
            title={this.props.numDealsInTotal + ' ' + hitsLabel + ' found'}
          />
        </div>
        <div>
          {items}
        </div>
      </div>
    );
  }
}

DealsFoundPanel.propTypes = {
  deals: PropTypes.arrayOf(PropTypes.instanceOf(Deal)).isRequired,
  numDealsInTotal: PropTypes.number.isRequired,
};