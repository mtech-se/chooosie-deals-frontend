import { Grid } from '@material-ui/core/es/index';
import queryString from 'query-string';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { BaseProgressLoader } from '../../components/BaseProgressLoader';
import { DealFactory } from '../../factories/DealFactory';
import { SpringPaginationFactory } from '../../factories/SpringPaginationFactory';
import { PaginationRequestParams } from '../../models/PaginationRequestParams';
import { SearchParams } from '../../models/SearchParams';
import { DealService } from '../../services/DealService';
import { getPaginationParamsFromQueryString, getSearchParamsFromQueryString } from '../../utils/UrlUtils';
import { FiltersPanel } from './FiltersPanel';
import { SearchResultsPanel } from './SearchResultsPanel';

class BaseSearchResultsView extends Component {
  constructor(props) {
    super(props);

    const searchParams = getSearchParamsFromQueryString(this.props.location.search);
    const paginationRequestsParams = getPaginationParamsFromQueryString(this.props.location.search);

    this.state = {
      hasFetchedData: false,
      deals: undefined,
      dotmDeals: undefined,

      searchTerm: searchParams.searchTerm,
      countryId: searchParams.countryId,
      locationId: searchParams.locationId,
      cuisineId: searchParams.cuisineId,
      priceBegin: searchParams.priceMin,
      priceEnd: searchParams.priceMax,
      categoryId: searchParams.categoryId,
      rangeMin: undefined,
      rangeMax: undefined,

      paginationRequestSize: paginationRequestsParams.size,
      paginationRequestPage: paginationRequestsParams.page,    // note: 0-based index i.e. 0 is page 1

      paginationResponseTotal: undefined,
      paginationResponsePerPage: undefined,
      paginationResponseCurrentPage: undefined,
    };

    this.onFiltersChange = this.onFiltersChange.bind(this);
    this.onPaginationNavigation = this.onPaginationNavigation.bind(this);
  }

  async componentDidMount() {
    const searchParams = this.getSearchParamsFromState();
    const paginationRequestsParams = new PaginationRequestParams(this.state.paginationRequestSize, this.state.paginationRequestPage);
    await this.makeFetchDealsPromise(searchParams, paginationRequestsParams);

    await this.makeFetchDotmDealsPromise();
  }

  componentDidUpdate() {
    const searchTermFromQueryParams = queryString.parse(this.props.location.search).searchTerm;

    if (this.state.searchTerm !== searchTermFromQueryParams && this.state.hasFetchedData) {
      this.setState({
        hasFetchedData: false,
        searchTerm: searchTermFromQueryParams,
      }, async () => {
        const searchParams = this.getSearchParamsFromState();
        const paginationRequestsParams = new PaginationRequestParams(this.state.paginationRequestSize, this.state.paginationRequestPage);
        await this.makeFetchDealsPromise(searchParams, paginationRequestsParams);
      });
    }
  }

  /**
   *
   * @param {SearchParams} searchParams
   * @param {PaginationRequestParams} paginationParams
   * @returns {Promise<void>}
   */
  async makeFetchDealsPromise(searchParams, paginationParams) {
    const combinedParams = {
      title: searchParams.searchTerm,
      price_min: searchParams.priceMin === undefined ? undefined : Math.round(searchParams.priceMin),
      price_max: searchParams.priceMax === undefined ? undefined : Math.round(searchParams.priceMax),
      category_id: searchParams.categoryId,
      size: paginationParams.size,
      page: paginationParams.page,
    };

    const response = await DealService.searchDeals(combinedParams);

    const responseHasNoDeals = response.data.content.length === 0;

    const deals = responseHasNoDeals ? [] : DealFactory.createFromJsonArray(response.data.content);
    const pagination = SpringPaginationFactory.createFromJson(response.data);

    this.setState({
      hasFetchedData: true,
      deals,
      paginationResponseTotal: responseHasNoDeals ? 0 : pagination.totalElements,
      paginationResponsePerPage: responseHasNoDeals ? undefined : pagination.size,
      paginationResponseCurrentPage: responseHasNoDeals ? undefined : pagination.number,
    });
  }

  async makeFetchDotmDealsPromise() {
    const response = await DealService.getDealsOfTheMonth(5, 0);

    const dotmDeals = DealFactory.createFromJsonArray(response.data.content);

    this.setState({
      dotmDeals,
    });
  }

  /** @param {SearchParams} searchParams */
  onFiltersChange(searchParams) {
    this.props.history.replace('/deals?' + searchParams.toUrlComponent());
    const paginationRequestsParams = new PaginationRequestParams(this.state.paginationRequestSize, this.state.paginationRequestPage);

    this.setState({
      hasFetchedData: false,
      searchTerm: searchParams.searchTerm,
      countryId: searchParams.countryId,
      locationId: searchParams.locationId,
      cuisineId: searchParams.cuisineId,
      priceMin: searchParams.priceMin,
      priceMax: searchParams.priceMax,
      size: paginationRequestsParams.size,
      pageNumber: paginationRequestsParams.page,
    }, async () => {
      await this.makeFetchDealsPromise(searchParams, paginationRequestsParams);
    });
  }

  /** @param {PaginationRequestParams} paginationParams */
  onPaginationNavigation(paginationParams) {
    const searchParams = this.getSearchParamsFromState();

    this.setState({
      hasFetchedData: false,
      pageNumber: paginationParams.page,
      size: paginationParams.size,
    }, async () => {
      await this.makeFetchDealsPromise(searchParams, paginationParams);
      window.scrollTo(0, 0);
    });
  }

  getSearchParamsFromState() {
    const searchParams = new SearchParams();
    searchParams.searchTerm = this.state.searchTerm;
    searchParams.countryId = this.state.countryId;
    searchParams.locationId = this.state.locationId;
    searchParams.cuisineId = this.state.cuisineId;
    searchParams.priceMin = this.state.priceMin;
    searchParams.priceMax = this.state.priceMax;
    searchParams.categoryId = this.state.categoryId;
    return searchParams;
  }

  render() {
    const progressLoader = (
      <Grid item
            xs={12}
            style={{ margin: '32px 0' }}>
        <BaseProgressLoader/>
      </Grid>
    );

    const searchParams = this.getSearchParamsFromState();

    const contents = (
      <SearchResultsPanel
        deals={this.state.deals}
        dotmDeals={this.state.dotmDeals}
        numRecordsInTotal={this.state.paginationResponseTotal}
        numRecordsPerPage={this.state.paginationResponsePerPage}
        currentPageNumber={this.state.paginationResponseCurrentPage}
        searchParams={searchParams}
        onPaginationNavigation={this.onPaginationNavigation}
      />
    );

    return (
      <Grid container
            style={{ marginTop: '16px', padding: '0 128px' }}>

        <Grid item
              xs={12}
              style={{ marginTop: '16px' }}>
          <FiltersPanel
            onChange={this.onFiltersChange}
            searchTerm={this.state.searchTerm}
            countryId={this.state.countryId}
            locationId={this.state.locationId}
            cuisineId={this.state.cuisineId}
            priceBegin={this.state.priceMin}
            priceEnd={this.state.priceMax}
            rangeMin={this.state.rangeMin}
            rangeMax={this.state.rangeMax}
            history={this.props.history}
          />
        </Grid>

        {this.state.hasFetchedData === false ? progressLoader : contents}

      </Grid>
    );
  }
}

BaseSearchResultsView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  location: ReactRouterPropTypes.location.isRequired,
};

export const SearchResultsView = withRouter(BaseSearchResultsView);