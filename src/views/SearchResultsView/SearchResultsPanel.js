import { Grid } from '@material-ui/core/es/index';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { DotmDealsPanel } from '../../components/DotmDealsPanel';
import { PaginationBar } from '../../components/PaginationBar/PaginationBar';
import { Deal } from '../../models/Deal';
import { SearchParams } from '../../models/SearchParams';
import { DealsFoundPanel } from './DealsFoundPanel';

export class SearchResultsPanel extends Component {
  render() {
    return (
      <Grid item
            container
            xs={12}
            style={{ marginTop: '16px' }}>
        <Grid item
              xs={1}
              style={{ marginTop: '40px' }}/>

        <Grid item
              xs={7}
              style={{ marginTop: '40px' }}>
          <DealsFoundPanel
            deals={this.props.deals}
            numDealsInTotal={this.props.numRecordsInTotal}
          />

          {this.props.deals.length !== 0 &&
          <div style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '64px',
          }}>
            <PaginationBar
              numRecordsInTotal={this.props.numRecordsInTotal}
              numRecordsPerPage={this.props.numRecordsPerPage}
              currentPageNumber={this.props.currentPageNumber}
              searchParams={this.props.searchParams}
              onPaginationNavigation={this.props.onPaginationNavigation}
            />
          </div>
          }
        </Grid>

        <Grid item
              xs={4}
              style={{ marginTop: '40px' }}>
          <DotmDealsPanel deals={this.props.dotmDeals}/>
        </Grid>
      </Grid>
    );
  }
}

SearchResultsPanel.propTypes = {
  deals: PropTypes.arrayOf(PropTypes.instanceOf(Deal)),
  dotmDeals: PropTypes.arrayOf(PropTypes.instanceOf(Deal)),
  numRecordsInTotal: PropTypes.number,
  numRecordsPerPage: PropTypes.number,
  currentPageNumber: PropTypes.number,
  searchParams: PropTypes.instanceOf(SearchParams),
  onPaginationNavigation: PropTypes.func.isRequired,
};