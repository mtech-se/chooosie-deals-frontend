import * as queryString from 'query-string';
import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ActionButton } from '../DealDetailsView/ActionButton';

export class BaseCheckoutSuccessView extends React.Component {
  render() {
    const queryParams = queryString.parse(this.props.history.location.search);

    const { payment_amount: paymentAmount, payment_id: paymentId } = queryParams;

    return (
      <div>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'space-around',
          margin: '40px auto',
          backgroundColor: '#fffffe',
          width: '1000px',
          height: '500px',
          filter: 'drop-shadow(0px -1px 13px rgba(0,0,0,0.26))',
        }}>
          <div style={{
            fontSize: '39px',
            color: '#b21b76',
            fontWeight: 'SemiBold',
          }}>
            Thank you!
          </div>
          <div style={{
            textAlign: 'center',
            fontSize: '14px',
            color: '#4a4a4a',
            fontWeight: 'Medium'
          }}>
            <p>
              Your payment of {paymentAmount} has been processed successfully! An order confirmation has been send to:
              ain@chooosie.com.
            </p>
            <p>
              Your order reference number is {paymentId}
            </p>
          </div>
          <div style={{
            width: '230px'
          }}>
            <ActionButton
              label={'Continue Shopping'}
              width={'100%'}
              color={'#eeeeee'}
              backgroundColor={'#601e59'}
              fontSize={'18px'}
              onClick={() => this.props.history.push('/')}
            />
          </div>
        </div>
      </div>
    );
  }
}

BaseCheckoutSuccessView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const CheckoutSuccessView = withRouter(BaseCheckoutSuccessView);