import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { AppBrand } from '../../components/AppBrand/AppBrand';
import { BaseProgressLoader } from '../../components/BaseProgressLoader';
import { LabelField } from '../../components/LabelField/LabelField';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { logInCustomer } from '../../store/actions/action-creators';

class BaseLoginView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: 'customer@customer.com',
      password: 'secret',
    };

    this.onSignInButtonClick = this.onSignInButtonClick.bind(this);
  }

  async onSignInButtonClick() {
    this.setState({
      isSignInButtonDisabled: true,
    });

    await this.props.logInCustomer(this.state.email, this.state.password);

    this.setState({
      isSignInButtonDisabled: false,
    }, () => this.props.history.push('/'));
  }

  render() {
    const content = this.state.isSignInButtonDisabled ? <BaseProgressLoader loadingMessage={'Signing in...'}/> : (

      <div style={{
        width: '400px',
        // display: 'flex',
        // flexDirection: 'column',
        // justifyContent: 'center',
      }}>
        <AppBrand
          imageHeight={'56px'}
          imageMargin={'8px'}
          style={{
            borderRadius: '8px 8px 0 0',
          }}
        />

        <div style={{
          padding: '16px',
          borderRadius: '0 0 8px 8px',
          backgroundColor: ColorConstants.faintGray,
        }}>

          <div
            style={{
              justifyContent: 'center'
            }}
          >
            <PanelTitle
              title={'Already Registered?'}
              marginBottom={'16px'}
            />
          </div>

          <LabelField
            title={'Email'}
            type={'text'}
            value={this.state.email}
            onChangeHandler={event => this.setState({ email: event.target.value })}
          />

          <LabelField
            title={'Password'}
            type={'password'}
            value={this.state.password}
            onChangeHandler={event => this.setState({ password: event.target.value })}
          />

          <div style={{
            display: 'flex',
            alignItems: 'center',
            marginTop: '16px',
          }}>
            <StylisedButton
              title={'Sign in'}
              onClick={this.onSignInButtonClick}
              disabled={this.state.isSignInButtonDisabled}
              colour={ColorConstants.teal}
            />

            <Link
              to={'/'}
              style={{
                marginLeft: '16px',
                fontFamily: 'Roboto, sans serif',
                color: ColorConstants.translucentBlack,
                fontSize: '0.8em',
              }}
            >Browse as guest</Link>

            <Link
              to={'/register'}
              style={{
                marginLeft: '16px',
                fontFamily: 'Roboto, sans serif',
                color: ColorConstants.translucentBlack,
                fontSize: '0.8em',
              }}
            >New to Halal Deals?</Link>
          </div>
        </div>
      </div>
    );

    return (
      <div style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        {content}
      </div>
    );
  }
}

BaseLoginView.propTypes = {
  history: PropTypes.object,
  logInCustomer: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    logInCustomer: (email, password) => dispatch(logInCustomer(email, password))
  };
};

export const LoginView = connect(null, mapDispatchToProps)(withRouter(BaseLoginView));