import { shallow } from 'enzyme';
import React from 'react';
import { LoginView } from './LoginView';

describe('LoginView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<LoginView/>);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});