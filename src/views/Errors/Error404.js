import React from 'react';
import logo from '../../assets/images/error-404-star-wars.gif';

const Error404 = () => (
  <div style={{
    width: '100vw',
    height: '100vh',
    textAlign: 'center',
  }}>
    <img
      src={logo}
      alt="404 - Page Not Found"
      style={{
        marginTop: '15vh',
      }}
    />
  </div>
);

export default Error404;