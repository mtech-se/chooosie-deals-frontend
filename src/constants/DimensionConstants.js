export const DimensionConstants = {
  topNavBarHeight: '64px',

  dealHeroImageWidth: '1000px',
  dealHeroImageHeight: '500px',

  dealGalleryMainImageWidth: '782.656px',
  dealGalleryMainImageHeight: '367.406px',

  dealCardImageWidth: '224px',
  dealCardImageHeight: '151px',

  dealCardLandscapeSmallImageWidth: '148px',
  dealCardLandscapeSmallImageHeight: '100px',
};