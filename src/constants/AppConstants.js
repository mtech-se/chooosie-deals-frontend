const APP_TITLE = 'Chooosie Deals Admin Dashboard';

export default class AppConstants {
  static get APP_TITLE() {
    return APP_TITLE;
  }
}